# state/room

This is where room states will be written during runtime


## Binary Device states

name: <room>/<device name>
contents:
<state>

Example
name: Office/Office AC
contents:
off


### Humidity

name: <room>/humidity
contents:
<hum>%

Example
name: Office/humidity
contents:
45%


### Temperature Control

Important: this file does not control temp manager. It only stores the active state

name: <room>/temp_control
contents:
<active|inactive>

Example: Office/temp_control
contents:
active


### Temperature

name: <room>/temperature
contents:
<temp><C|F>

Example
name: Office/temperature
contents:
70F
