# State files

This readme should document the expected format of all state files


## Room specific state

All room specific state will be written to state/room. Refer to that directory for format


## Network

All network state should be written to state/network. Refer to that directory for format


## Service

All service state should be written to state/service. Refer to that directory for format


## Outside state

All outside states, i.e. temperature, should be written to state/outside.
