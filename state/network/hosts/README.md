# state/network/hosts

This is where host availability will be written during runtime

## Hosts

name: <hostname>
contents:
<active|inactive>

Example
name: myhost
contents:
active
