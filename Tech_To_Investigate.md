# Tech To Investigate

This doc contains references to cool tech (HW or SW) that we should look at integrating into GlueOS


# Google Home Alerts

This project sends notifications through Google Home - i.e. Google Home will speak the notification. We should look at using this to incorporate emergency alerts (i.e. house security system).

http://www.notenoughtech.com/home-automation/google-home-notifications-tutorial/


# Google Home API

This is an undocumented Google Home API. It operates on the locate network (not through the internet). Uncertain if there's anything here we can use, but it's worth investigating.

http://www.androidpolice.com/2018/01/16/google-home-hidden-api-local-devices-can-use/
