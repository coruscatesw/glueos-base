#!/bin/bash

#
# This is a (space separated) array of device types. Only include a device type 
# here if devices of that type are to be controlled.
DEVICE_TYPES=( ZWAY )

#
# This is a (line separated) array of device name, device room pairs controlled
# via a ZWay controller should be added here
DEVICES["ZWAY"]="Main Light,Office
Office AC,Office
Monitors,Office"
