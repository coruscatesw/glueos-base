#!/bin/bash

#
# the match threshold is used for fuzzy matching hmi input. If the best match of
# a translation is at or below this threshold, nothing will be executed.
#
MATCH_THRESHOLD=90

#
# each translation defines the hmi that will trigger it (space separated array
# of potential input strings) and what will be triggered (array key). The key 
# either has to match a built-in service, or specify the name of a custom script
# which shall be located in scripts/custom
#

#
# The translations can contain parameters.  Currently there can only be a
# single parameter.  The parameters may have spaces in them and are case
# insensitive.
# EG:
# Valid:  turn on %DEVICE%
# Valid:  %ACTION% device
# Valid:  turn %action% device
# Valid:  perform %DEVICE ACTION% on network

# built in outside temp requests
TRANSLATION["outside_temp_request"]="what is the temperature outside
what is the outside temperature"

# built in external ip request
TRANSLATION["external_ip_request"]="what is my external ip
what is my ip"

# built in device control request
TRANSLATION["device_control_request"]="turn %STATE% the %DEVICE%"

# built in wake on lan request
TRANSLATION["wake_on_lan_request"]="wake up %COMPUTER%"

# built in duck dns renew request
TRANSLATION["renew_duckdns_request"]="renew duckdns
renew duck dns"

# built in temp control request
TRANSLATION["temp_control_request"]="set temp control in %ROOM% to %STATE%"

# example of a custom script
TRANSLATION["custom_script_example.sh"]="run my custom script
execute my custom script
be awesome"

TRANSLATION["custom_script_with_params_example.sh"]="dance to my %dance% beat"
