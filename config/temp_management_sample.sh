#!/bin/bash

#
# List all rooms for which temperature management should be performed. For each
# room specified, included the block of params below. This is a comma-separated
# list
ROOMS_CONTROLLED="Office"

#
# specify these variables for each room to be controlled
#  HEAT_DEVICES and AC_DEVICES are both a comma separated list of device names
#   for heat and AC. These can be empty, but the still have to be included. The
#   device names and rooms must match those in the devices config
#  ACTIVE_TEMP_RANGE is the temp range that will be maintained if temperature
#   management is active
#  INACTIVE_TEMP_RANGE is the temp range that will be maintained if temperature
#   management is inactive
#  VARIANCE provides a little wiggle room for (de)activating heat/ac devices to
#   reduce toggling
HEAT_DEVICES["Office"]=
AC_DEVICES["Office"]="AC Switch 1","AC Switch 2"
ACTIVE_TEMP_RANGE["Office"]=72-78
INACTIVE_TEMP_RANGE["Office"]=45-90
VARIANCE["Office"]=1
