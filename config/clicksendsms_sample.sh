#!/bin/bash

#
# AUTH_KEY comes from combining your username and password for clicksend into a
# "username:password" string (: included) and the base64 encoding. I.e.:
#
#   printf "myusername:mypassword" | base64
#   > bXl1c2VybmFtZTpteXBhc3N3b3Jk
#
AUTH_KEY=r3dAcT3d

#
# DEST_NUM is the number to which the notifications go. This must be approved in
# your clicksend settings
#
DEST_NUM="+15555555555"

#
# MSG_HEAD is optional text to preceed the notification. Useful for identifying
# where the notification originated
#
MSG_HEAD="Precursor"
