#!/bin/bash

# This is the list of containers to monitor on the host. If a container becomes
# unavailable, an alert will be sent
#
# NOTE: passwordless access to run 'sudo docker ps -a' must be granted for the
# user running this script, else it will hang at a sudo password prompt
#
CONTAINERS_TO_MONITOR="container1
container2
"
