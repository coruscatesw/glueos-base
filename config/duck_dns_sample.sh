#!/bin/bash

#
# DuckDNS token and domain(s) can be obtained by logging in to duckdns.org
# DOMAINS is a comma separated list (if more than 1 domain)
#
TOKEN=53cr3t_t0k3n
DOMAINS=mydomain1,mydomain2
