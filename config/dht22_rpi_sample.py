#
# This file identifies the sensor type and GPIO pin used for DHT22 temp sensors
# connected to raspberry pis
#

import Adafruit_DHT

# Note: the Adafruit library technically supports the DHT11, DHT22, and AM2302.
# However, only the DHT22 has been tested.
# Sensor should be set to Adafruit_DHT.DHT11, Adafruit_DHT22, or Adafruit_AM2302
sensor = Adafruit_DHT.DHT22

# DHT sensor connected to pin 22.
pin = 22

# Room name for collected temperature
room = Office
