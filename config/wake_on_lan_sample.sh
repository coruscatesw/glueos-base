#!/bin/bash

#
# WOL_HOSTS is a list of hostname,mac_address pairs
# BROADCAST is the broadcast address for the magic packet
# UDP_PORT is the port to send the packet on
# 

WOL_HOSTS="host1,01:23:45:67:89:AB
host2,AB:89:67:45:23:01"
BROADCAST=255.255.255.255
UDP_PORT=9
