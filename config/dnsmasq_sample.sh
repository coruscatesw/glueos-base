#!/bin/bash

# This defines the network to monitor & report. If a lease is made on another
# network, it will not be reported
NET_OF_INTEREST="192.168.32"

# This range defines the IPs that will be reported. If a lease falls outside
# of this range, an alert will not be sent. Example usage would be to only
# report the range for which dnsmasq is set to dynamically assign. I.e. skip
# any static, known hosts
BEGIN_RANGE="129"
END_RANGE="200"
