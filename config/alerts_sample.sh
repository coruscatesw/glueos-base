#!/bin/bash

# set ALERTER to the name(s) of the alert script(s) to use for alerts. Scripts
# should be located in glueos-base/scripts/alerts.

# single alerter example
ALERTER=( join.sh )
#ALERTER=( autoremote.sh )
#ALERTER=( clicksendsms.sh )

# multiple alerter example
#ALERTER=( autoremote.sh join.sh )

# These binary (yes|no) variables define which alert types will be sent
# Note: custom types can be added, but also must be implemented where invocation
# is desired
# Built-in alert type descriptions:
#  feedback - alerts sent in response to a user request
#  security - alerts sent on specific triggers, i.e. along the lines of security
#  info - informational alerts sent about glueos operation, most likely used 
#         for debug
#  error - error alerts sent about glueos operation, sent when scripts or 
#          functionality fail for any reason
show_alerts["feedback"]=yes
show_alerts["security"]=yes
show_alerts["info"]=no
show_alerts["error"]=yes
