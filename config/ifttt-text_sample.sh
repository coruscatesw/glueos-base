#!/bin/bash

# This is the configuration file for IFTTT incorporation.

# In order to receive notifications through IFTTT, you have to have configured
# the Webhooks service. EVENT is the name of the event you created from which
# notifications will be received. MAKER_KEY is your web hooks key.
EVENT=glueos_feedback
MAKER_KEY=blahsometlongstringredacted

# An example IFTTT rule to forward glueos notifications through IFTTT via SMS:
#  IF This: Receive a web request
#     Event Name: "glueos_feedback" (this is EVENT above, set to anything)
#  Then That: Send me an SMS
#     Message: GlueOS:: {{Value1}}: {{Value2}}
#
#  Value1 of the Then That will be the title; Value2 will be the message.
