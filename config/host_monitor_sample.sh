#!/bin/bash

# This is the list of hosts to monitor on the network. If a host becomes
# unavailable, an alert will be sent
#
# Note: adding a pingable external IP, such as google.com, serves as a check
# for internet access.
HOSTS_TO_MONITOR="host1
host2
host3
google.com"
