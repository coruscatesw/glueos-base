# Config Files

Only sample files are provided for configurations. This allows you to create your custom configs in place without accidentally pushing them to the repo. To create your config, create a copy of the sample config and remove '_sample' from the name.
