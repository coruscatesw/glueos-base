# Periodic recurring tasks

Periodic recurring tasks should be added to crontab. To do so, run:

crontab -e

The crontab entry format is:

minute hour day_of_month month day_of_week command

There is a lot of documentation easily available about cron. If you need to know
how to schedule tasks at a specific interval, Google is your friend.

A few crontab example entries:

Execute a task every time the computer starts up:
@reboot /path/to/command.sh

Execute a task every hour:
0 * * * * /path/to/command.sh

Execute a task at 5pm every Tuesday:
0 5 * * 2 /path/to/command.sh


# One time tasks

One time tasks can be added manually via the 'at' command. Again, Google is
your friend for specific examples. However, here is a quick example:

at 9:03AM
> /path/to/command.sh
(ctrl+d)


# Dynamic scheduling

@TODO
