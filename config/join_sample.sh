#!/bin/bash

#
# Users of Join have an API key assigned and a device ID for each device 
# registered to receive notifications. Refer to the Join site for instructions
# on how to get your device IDs and API key: https://joinjoaomgcd.appspot.com/
# Note: select a device and click Join API
#

# These fields need to be specified for each user that wishes to receive alerts.
DEVICEIDS["user1"]=commaSeparatedList,ofDeviceIDS,toReceiveAlerts
APIKEY["user1"]=insertAPIKeyHere

# This is an example of a second user to alert
DEVICEIDS["user2"]=deviceIDsOfUser2,stillCommaSeparated
APIKEY["user2"]=insertUser2APIKeyHere
