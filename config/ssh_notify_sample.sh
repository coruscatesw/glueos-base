#!/bin/bash

# if this option is set, alerts will only be sent for SSH connections established
# from other networks. This is useful for limiting notifications to external 
# connections
EXCLUDE_NETWORK="192.168.1."

# if this option is set, alerts will only be sent for SSH connections established
# by other user(s)
EXCLUDE_USERS=( user1 user2 )

# if this option is set, alerts will not be sent for SSH connections from these
# originating IPs
EXCLUDE_IPS=( 192.168.2.12 192.168.2.16 )
