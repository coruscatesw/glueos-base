#!/bin/bash

#
# Users of AutoRemote have a key assigned. Refer to the AutoRemote site for 
# instructions on how to get your key:  https://joaoapps.com/autoremote/direct/
#

# This field needs to be populated for each user/device that wishes to receive alerts
AR_KEY["user1"]=S0m3_K3y_Fr0m_UR7

# This is an example of a second user to alert
AR_KEY["user2"]=I_1Ik3_pIck135
