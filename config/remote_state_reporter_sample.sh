#!/bin/bash

# This is the config for the remote state reporter, which copies state updates
# to remote nodes.

# These binary (yes|no) states determine whether states under these directories
# will be copied.
NETWORK=yes
OUTSIDE=yes
ROOM=yes
SERVICE=no

#
# This (comma separated) list defines what room states will be copied to remote 
# nodes. Requires ROOM=yes above
#
ROOMS="Office,Living Room,Study"

#
# This (comma separated) list defines what remote nodes the states will be 
# copied to. Important note: passwordless SSH is expected to be configured for 
# each of this nodes. If it is not, remote reporting will fail
#
REMOTE_NODES="secondhost,thirdhost"
