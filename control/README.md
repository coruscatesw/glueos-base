# Control files

This readme should document the expected format of all control files


## Device query files

name: device_query_<type of device>
contents:
<empty or names of devices to query>

<type of device> in the name should be a supported device type (i.e. 'zway'), or 'all'

If the contents of the file is empty, all devices of that type will be queried

If the contents of the file is not empty, it shall list the devices (one per line) to be queried.

Example
name: device_query_zway
contents (empty):


## Device control files

name: device_<uuid>
contents:
<device name>
<requested state>

Example

name: device_1517491915
contents:
Office AC
on


## Temperature control files

These files active/deactive temperature management

name: temp_<uuid>
contents:
<room>
<active|inactive>

Example
name: temp_cb8a59cc-f5f4-4761-83d3-09b59df91f19
contents:
Office
active


## Wake On Lan control files

These files trigger wake on lan requests

name: wol_<uuid>
contents:
<wake on lan hostname>

Example
name: wol_448d3426-a36e-47cc-9805-045216bee087
contents:
computer1


## Duck DNS Renewal control files

These files trigger a Duck DNS renewal

name: duckdns_renewal_<uuid>
contents:
<empty>

Example
name: duckdns_renewal_9f1fdf17-9825-4cc5-b4aa-9150a69f78b8
contents:


## HMI control files

These files provide raw HMI input to GlueOS

name: hmi_<uuid>
contents:
<raw hmi>

Example
name: hmi_9b24c2ce-66e8-4129-93d0-ca72d35f9996
contents:
what is the outside temperature
