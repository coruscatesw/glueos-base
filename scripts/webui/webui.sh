#!/bin/bash
##
# This script starts the web service. It listens for HMI via an http
# server, and provides a UI for information. HMI requests can be sent by passing 
# the 'cmd' variable, i.e.:
#   http://<my_external_ip>:$LISTENING_PORT/listen.php?cmd=this%20is%20my%20cmd
#
# The UI can be viewed in a web browser via:
#   http://<my_external_ip>:$LISTENING_PORT/index.php
#
# Returns 0
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
REQ_BINARIES=( php )
REQ_PACKAGES=( php-cli )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit webui.sh

#
# run the server and restart it if it unexpectedly dies
#
MYDIR="`dirname $0`"
until `php -S 0.0.0.0:${HTTP_PORT} -t "$MYDIR"` ; do
  log "unexpectedly closed with exit code $?; restarting..."
  sleep 1
done
log "shutting down..."
