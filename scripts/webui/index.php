<html>
  <head>
    <title>GlueOS UI</title>
    <link rel="stylesheet" href="glueos.css">
    <script src="glueos.js"></script>
  </head>
  <body onload="setTimeout(setShouldProcessImageUpdates,5000);">
<?php
require_once('./dir.php');

// functions to establish the initial html layout
function set_pre_html($path,$level) {
  switch ($level) {
    case 0: echo "<p>".basename($path)."</p><table><tr>"; break;
    case 3:
    case 2: echo "</tr><tr>";
    case 1: echo "<td><table class=\"lvl".$level."\"><tr><td><p>".basename($path)."</p></td></tr>"; break;
  }
}

function set_file_html($path,$dir) {
  if ($dir == "snapshot.jpg") {
    $contents = "image";
  } else {
    $contents=rtrim(file_get_contents($path."/".$dir));
  }
  $state=$contents;
  $is_binary=false;
  $id=pathname_to_id($path."/".$dir);

  echo "<tr><td id=\"".$id."\" class=\"state";
  if ($contents==="on" || $contents==="active") {
    echo " on";
    $is_binary=true;
  } else if ($contents==="off" || $contents==="inactive") {
    echo " off";
    $is_binary=true;
  } else if (substr($dir,0,5)==="zway_") {
    $state="redacted";
  } 

  if ($is_binary) {
    $btn_text="";
    $oc_params="";
    $visible = true;
    switch ($contents) {
      case "on":
        $btn_text="Turn Off";
        $oc_params="controlDevice('$dir','off')";
        break;
      case "off":
        $btn_text="Turn On";
        $oc_params="controlDevice('$dir','on')";
        break;
      default:
        if (stristr($path,"/hosts")) {
          $btn_text="Send WoL";
          $oc_params="wakeOnLan('$dir')";
          if ($contents === "active") {
            $visible = false;
          }
        } else if ($dir==="duck_dns") {
          $btn_text="Renew";
          $oc_params="renewDuckDns()";
          if ($contents === "active") {
            $visible = false;
          }
        } else if ($dir==="temp_control") {
          $room=substr($path,strpos($path,"room")+5);

          if ($contents==="active") {
            $btn_text="Deactivate";
            $oc_params="controlTemp('".$room."','inactive')";
          } else {
            $btn_text="Activate";
            $oc_params="controlTemp('".$room."','active')";
          }
        } else if (stristr($path,"/service")) {
          $visible = false;
        }
        break;
    }

    $onclick=$oc_params.";";

    $btn="<button id=\"".$id."_btn\" type=\"button\" onclick=\"".$onclick."\" ".($visible?"":"style=\"display: none;\"").">".$btn_text."</button>";
  }
  if ($contents=="image") {
    echo "\"><a target=\"_blank\" href=\"image.php?img=$path/$dir\"><img class=\"thumbnail\" src=\"image.php?img=$path/$dir\"></img></a></td></tr>";
  } else {
    echo "\">$dir".($is_binary?" $btn":": $state")."</td></tr>";
  }
}

function set_post_html($level) {
  switch ($level) {
    case 0: echo "</tr></table>"; break;
    case 1:
    case 2:
    case 3: echo "</table></td>"; break;
  }
}

ls_dir('../../state',0,"set_pre_html","set_post_html","set_file_html");

?>
  <div id="hidden" hidden></div>
  <div id="result_head">Debug log</div>
  <div id="result"></div>
  </body>
</html>
