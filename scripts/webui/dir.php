<?php

function pathname_to_id($pathname) {
  return str_replace("/","__",$pathname);
}

function ls_dir($path,$level,$pre_func,$post_func,$file_func) {
  call_user_func($pre_func,$path,$level);

  // ignore . and .. entries
  $dirs = array_slice(scandir($path),2);
  foreach ($dirs as $dir) {
    // ignore a few files
    if ($dir=="README.md" || $dir==".gitignore") {
      continue;
    }   

    // if this is a directory, recurse
    if (is_dir($path."/".$dir)) {
      ls_dir($path."/".$dir,$level+1,$pre_func,$post_func,$file_func);
    } else {
      call_user_func($file_func,$path,$dir);
    }
  }

  call_user_func($post_func,$level);
}

?>
