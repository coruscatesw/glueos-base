<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

require_once('./dir.php');

function do_nothing() {
}

function check_file_for_update($path,$dir) {
  $checkIntervalSeconds = 5;
  $update = (time()-filemtime($path."/".$dir) < $checkIntervalSeconds);

  if ($update) {
    $stateUpdate = ($dir!="snapshot.jpg");
    // specifiy unique ID & next check interval
    echo "id: ".uniqid()."\n";
    echo "retry: ".($checkIntervalSeconds*1000-100)."\n";
    echo "event: ".($stateUpdate?"stateUpdate":"imageUpdate")."\n";
    echo "data: {\n";
    echo "data: \"id\": \"".pathname_to_id($path."/".$dir)."\"".($stateUpdate?",":"")."\n";
    if ($stateUpdate) {
      $contents = rtrim(file_get_contents($path."/".$dir));
      echo "data: \"contents\": \"".$contents."\"\n";
    }
    echo "data: }\n\n";
    flush();
  }
}

// iterate over all state files, sending updates for only those that have changed
ls_dir("../../state",0,"do_nothing","do_nothing","check_file_for_update");

?>
