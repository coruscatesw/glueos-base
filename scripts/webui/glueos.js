var source = new EventSource("state_update_event.php");

function shouldProcessImageUpdates()
{
  return (document.getElementById("hidden").innerHTML === "process");
}

function setShouldProcessImageUpdates() {
  document.getElementById("hidden").innerHTML="process";
}

source.addEventListener('imageUpdate', function(event) {
  if (!shouldProcessImageUpdates()) {
    setShouldProcessImageUpdates();
    return;
  }

  var data = JSON.parse(event.data);
  id=data.id;
  
  //@TODO - update the image. For now, just 
  // force a reload
  location.reload(true);
});

source.addEventListener('stateUpdate', function(event) {
  var data = JSON.parse(event.data);
  id=data.id;
  content=data.contents;

  // determine what type of data was updated
  binary_switch = false;
  binary_state  = false;
  if (content === "on" || content === "off") {
    binary_switch = true;
  } else if (content === "active" || content === "inactive") {
    binary_state = true;
  }
  binary = binary_switch || binary_state;

  var element = document.getElementById(id);
  if (element) {
    if (binary) {
      // binary elements don't require an innerHTML update, only a class update
      // and a button update
      element.className = "state "+content;
      var button = document.getElementById(id+"_btn");
      var onclick = button.getAttribute("onclick");

      switch (content) {
        case "on":
          button.innerHTML = "Turn Off";
          button.setAttribute("onclick",onclick.substr(0,onclick.indexOf(","))+",'off');");
          break;
        case "off":
          button.innerHTML = "Turn On";
          button.setAttribute("onclick",onclick.substr(0,onclick.indexOf(","))+",'on');");
          break;
        default:
          if (id.indexOf("temp_control") !== -1) {
            if (content === "active") {
              button.innerHTML = "Deactivate";
              button.setAttribute("onclick",onclick.substr(0,onclick.indexOf(","))+",'inactive');");
            } else {
              button.innerHTML = "Activate";
              button.setAttribute("onclick",onclick.substr(0,onclick.indexOf(","))+",'active');");
            }
          } else if (id.indexOf("__hosts__") !== -1 || id.indexOf("duck_dns") !== -1) {
            if (content === "active") {
              button.setAttribute("style","display: none;")
            } else {
              button.setAttribute("style","");
            }
          }
          break;
      }
    } else if (id.indexOf("zway_") == -1 ) {
      var parts = id.split("__"),
        name=parts[parts.length-1];
      element.innerHTML = name + ": " + content;
    }
  } else {
    // a new state has been reported. Force a page reload from the server
    location.reload(true);
  }

  var oldhtml = document.getElementById("result").innerHTML; 
  document.getElementById("result").innerHTML = Math.round(Date.now()/1000) + " id: "+id+",c: "+content + "<br/>" + oldhtml;
});

function controlDevice(device,state) {
  var xmlHttp = new XMLHttpRequest();
  var cmd = encodeURIComponent("turn "+state+" the "+device);
  xmlHttp.open("GET","listen.php?cmd="+cmd,true);
  xmlHttp.send(null);
}

function controlTemp(room,state) {
  var xmlHttp = new XMLHttpRequest();
  var cmd = encodeURIComponent("set temp control in "+room+" to "+state);
  xmlHttp.open("GET","listen.php?cmd="+cmd,true);
  xmlHttp.send(null);
}

function wakeOnLan(device) {
  var xmlHttp = new XMLHttpRequest();
  var cmd = encodeURIComponent("wake up "+device);
  xmlHttp.open("GET","listen.php?cmd="+cmd,true);
  xmlHttp.send(null);
}

function renewDuckDns() {
  var xmlHttp = new XMLHttpRequest();
  var cmd = encodeURIComponent("renew duck dns");
  xmlHttp.open("GET","listen.php?cmd="+cmd,true);
  xmlHttp.send(null);
}
