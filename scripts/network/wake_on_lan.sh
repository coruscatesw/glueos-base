#!/bin/bash
##
# This script sends wake on lan packets
#  arg1 = host to send WoL packet to
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( curl cut nc sed )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file that contains alerter
#
include_config_or_exit wake_on_lan.sh

SUCCESS=-1

#
# find MAC for host and send
#
for entry in $WOL_HOSTS
do
  host=$(echo "$entry" | cut -d',' -f1)
  if [ "$host" == "$1" ]
  then
    mac=$(echo "$entry" | cut -d',' -f2)
    log "sending wol packet for host <$host>, mac <$mac>"

    echo -e $(echo $(printf 'f%.0s' {1..12}; printf "$(echo $mac | sed 's/://g')%.0s" {1..16}) | sed -e 's/../\\x&/g') | nc -w1 -u -b $BROADCAST $UDP_PORT

    SUCCESS=0
  fi
done

exit $SUCCESS
