#!/bin/bash
##
# This script is used to query the external IP
#  No args passed in
#  Does not require config file
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
REQ_BINARIES=( curl dig )
REQ_PACKAGES=( dnsutils )
. `dirname $0`/../general/verify_dependencies.sh
# include globals; pass reference to glueos-base directory
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# script specific variables
#
EXT_IP_FILE=${GLUEOS_STATE_DIR}/network/extip

# Get last stored external IP
CURRENT_EXT_IP=""
if [ -e "$EXT_IP_FILE" ] ; then
  CURRENT_EXT_IP=$(cat $EXT_IP_FILE)
fi

# Check current external IP
EXT_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)

if [ $? != 0 ] ; then
  ERR_STR="error getting external IP"
  log "$ERR_STR"
  alertError "External IP Monitor" "$ERR_STR"
  exit -1
fi

# alert if IP changed
if [ "$CURRENT_EXT_IP" != "$EXT_IP" ] ; then
  alertSecurity "External IP change" "The external IP has changed from ${CURRENT_EXT_IP} to ${EXT_IP}"
fi

echo "${EXT_IP}" > "$EXT_IP_FILE"
