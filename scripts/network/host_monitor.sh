#!/bin/bash
##
# This script monitors hosts on the network and alerts if they become unavailable
#
# Returns 0
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
REQ_BINARIES=( ping )
. `dirname $0`/../general/verify_dependencies.sh
# include globals; pass reference to glueos-base directory
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file that contains alerter
#
include_config_or_exit host_monitor.sh

#
# ping the host(s)
#
for host in $HOSTS_TO_MONITOR
do
  success=$(ping -c1 -w1 $host)
  if [ $? != 0 ]
  then
    alertSecurity "Host unavailable" "$host is unreachable as of `date +%s`"
    set_host_state "$host" "inactive"
  else
    set_host_state "$host" "active"
  fi
done
