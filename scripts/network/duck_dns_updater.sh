#!/bin/bash
##
# This script renews your IP on duckdns.org for the specific domains
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
REQ_BINARIES=( curl )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file that contains alerter
#
include_config_or_exit duck_dns.sh

#
# renew
#
this_success=$(curl -G -s --data-urlencode "domains=$DOMAINS" --data-urlencode "token=$TOKEN" "https://www.duckdns.org/update?ip=")
SUCCESS=`echo "$success" | grep -q OK`

if [[ $SUCCESS == 0 ]] ; then
  echo "inactive" > "${GLUEOS_STATE_DIR}/network/duck_dns"
  alertInfo "duck_dns_updater: failed to update duck dns"
else
  echo "active" > "${GLUEOS_STATE_DIR}/network/duck_dns"
fi

exit $SUCCESS
