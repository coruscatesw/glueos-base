#!/bin/bash
##
# This script sends alerts when an SSH session is established to this host. In 
# order to work, /etc/pam.d/sshd must be modified to include the following line:
#  session optional pam_exec.so seteuid /home/<user>/glueos-base/scripts/network/ssh_login_notify.sh
#
# Variables set when called:
#  PAM_RHOST - IP of remote host logging in
#  PAM_USER  - username logging in
#  PAM_TYPE  - session type
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
. `dirname $0`/../general/verify_dependencies.sh
# include globals; pass reference to glueos-base directory
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit ssh_notify.sh


#
# This function verifies the remote IP is not excluded from alerts
#
verify_network() {
  ret=0

  if [[ -v EXCLUDE_NETWORK ]] && [[ $PAM_RHOST == $EXCLUDE_NETWORK* ]] ; then
    ret=-1
    logState "Not alerting of SSH login from $PAM_RHOST; excluded network"
  fi

  return $ret
}

#
# This function verifies the user is not excluded from alerts
#
verify_users() {
  ret=0

  if [[ -v EXCLUDE_USERS ]] ; then
    for user in "${EXCLUDE_USERS[@]}" ; do
      if [ "$user" == "$PAM_USER" ] ; then 
        ret=-1
        logState "Not alerting of SSH login by $PAM_USER; excluded user"
        break
      fi
    done
  fi

  return $ret
}

#
# This function verifies the IP is not excluded from alerts
#
verify_ips() {
  ret=0

  if [[ -v EXCLUDE_IPS ]] ; then
    for ip in "${EXCLUDE_IPS[@]}" ; do
      if [[ "$ip" == "$PAM_RHOST" ]] ; then
        ret=-1
        logState "Not alerting of SSH login from $PAM_RHOST; excluded IP"
        break
      fi
    done
  fi

  return $ret
}

#
# send alert for SSH login if it's not excluded via config
#
if [ "$PAM_TYPE" != "close_session" ]
then
  if verify_network && verify_users && verify_ips
  then
    alertSecurity "SSH Login on `hostname`" "Login by $PAM_USER from $PAM_RHOST to `hostname`"
  fi
fi
