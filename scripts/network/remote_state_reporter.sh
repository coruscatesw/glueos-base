#!/bin/bash
##
# This is the remote state reporter, which is responsible for copying state
# updates to remote nodes as specified in the config
#  arg1 = name of the state file that has updated
#
# Returns 0 if successful
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( scp ssh )
#REQ_PACKAGES=( ) # uncomment to require specific packages for execution
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit remote_state_reporter.sh

#
# script specific variables
#
SUCCESS=0
RELEVANT=-1

#
# main script code
#

#
#
copy_state_file_to_remotes() {
  # copy state to each configured remote node
  get_glueos_relative_pathname "$1" # sets RELATIVE_PATHNAME
  MDIR=${RELATIVE_PATHNAME%/*}
  split_string_on_delimiter "$REMOTE_NODES" ","
  for remote in ${WORDS[@]} ; do
    log "copying <$RELATIVE_PATHNAME> to <$remote>..."
    MKDIR_CMD=$(ssh $remote "mkdir -p ~/glueos-base/$MDIR")
    RV=$?
    if [ $RV != 0 ] ; then
      log "error making remote directory <$MDIR>:<$RV>; cannot copy state"
    else
      sanitize_string_for_remote_command "$RELATIVE_PATHNAME"
      CP_CMD=$(scp "$1" $remote:"~/glueos-base/$SANITIZED_STRING")
      RV=$?
      if [ $RV != 0 ] ; then
        log "error copying state file <$RV>"
      fi
    fi
  done
}

#
#
copy_room_state() {
  # parse the file name for the specific room
  get_room_from_filename "$1" # sets ROOM

  # parse configured rooms to determine whether update is applicable
  split_string_on_delimiter "$ROOMS" "," # sets WORDS
  for room in "${WORDS[@]}" ; do
    if [[ "$ROOM" == "$room" ]] ; then
      log "state update for <$room> will be copied..."
      RELEVANT=0
      break
    fi
  done
  if [[ $RELEVANT != 0 ]] ; then
    log "state update for <$ROOM> is not configured to be copied; ignoring"
    return
  fi

  copy_state_file_to_remotes "$1"
}

case "$1" in
*/state/room/*)
  if [[ -v ROOM ]] && [[ $ROOM == "yes" ]] ; then
    copy_room_state "$1"
  fi
  ;;
*/state/network/*)
  if [[ -v NETWORK ]] && [[ $NETWORK == "yes" ]] ; then
    copy_state_file_to_remotes "$1"
  fi
  ;;
*/state/outside/*)
  if [[ -v OUTSIDE ]] && [[ $OUTSIDE == "yes" ]] ; then
    copy_state_file_to_remotes "$1"
  fi
  ;;
*/state/service/*)
  if [[ -v SERVICE ]] && [[ $SERVICE == "yes" ]] ; then
    copy_state_file_to_remotes "$1"
  fi
  ;;
*)
  log "unhandled state update <$1>"
  SUCCESS=-1
  ;;
esac

exit $SUCCESS
