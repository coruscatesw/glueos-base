#!/bin/bash
##
# This script is intended to be used on hosts running dnsmasq. It will be run
# when a new DHCP lease is assigned or destroyed, or when the dnsmasq service is
# restarted. It will be passed the following params
#  arg1 = [add|del|old]
#    add = new lease granted
#    del = lease has been removed
#    old = lease is existing, happens on service restart
#  arg2 = MAC
#  arg3 = IP
#  arg4 = hostname (optional)
#
# To use, add the following line to your dnsmasq config , i.e. 
# /etc/dnsmasq.d/<config>.conf
#  dhcp-script=/path/to/script/dnsmasq_lease_alert.sh
##

#
# verify all dependecies for this script
#
. `dirname $0`/../general/verify_dependencies.sh
# include globals; pass reference to glueos-base directory
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit dnsmasq.sh

# rename variables passed in for convenience
export op=$1
export mac=$2
export ip=$3
export host=$4

export dbg="true"

if [ "${dbg}" = "true" ] ; then
  logState "${op} lease for MAC ${mac} with IP ${ip} and hostname ${host}"
fi

# check IP against config; send alert if IP falls in monitored range 
id=$(echo ${ip} | cut -d'.' -f4)
if [ "${op}" = "add" ] && [[ "${ip}" == "${NET_OF_INTEREST}"* ]] && [ ${id} -ge ${BEGIN_RANGE} ] && [ ${id} -le ${END_RANGE} ]; then
  alertSecurity "New Network Device Detected" "MAC ${mac}, IP ${ip}, host ${host}"
fi
