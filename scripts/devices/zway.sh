#!/bin/bash
##
# This script controls zway devices
#  arg1 = command [GET_AUTH|
#                  GET_DEVICES|
#                  GET_DEVICE_STATE|
#                  SET_DEVICE_STATE]
#  arg2 [Required for GET_DEVICE_STATE, SET_DEVICE_STATE]
#       = device name
#  arg3 [Required for SET_DEVICE_STATE]
#       = device state [on|off]
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
#REQ_NUM_ARGS=1
REQ_BINARIES=( curl jq grep cut )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit zway.sh

#
# script specific variables
# 
COOKIE_FILE=${GLUEOS_STATE_DIR}/service/zway_auth_cookie
DEVICE_FILE=${GLUEOS_STATE_DIR}/service/zway_devices

#
# This function gets an authorization cookie (required by all further commands)
# The auth cookie is stored in COOKIE_FILE
#

getAuthCookie() {
  SUCCESS=0

  # determine whether controller is reachable
  CMD=$(ping -c1 -w1 ${CONTROLLER_IP})
  if [[ $? != 0 ]] ; then
    SUCCESS=-1
    # controller unreachable. Try again for up to 10 seconds
    for i in `seq 1 10` ; do
      CMD=$(ping -c1 -w1 ${CONTROLLER_IP})
      if [[ $? == 0 ]] ; then
        SUCCESS=0
        break
      fi
    done
  fi

  # get cookie
  if [[ $SUCCESS == 0 ]] ; then
    auth_cookie=$(curl -s -H "Content-Type: application/json" -X POST -d '{"form":"true","login":"'${USER}'","password":"'${PASS}'","keepme":"false","default_ui":1}' -i http://${CONTROLLER_IP}:8083/ZAutomation/api/v1/login | grep -E "^Set-Cookie:" | cut -d' ' -f2,3,4)
    log "auth cookie obtained"
    echo "$auth_cookie" > $COOKIE_FILE
  else
    log "error retriving auth cookie; controller unreachable"
    log "rescheduling auth cookie retrieval in a minute or so"
    execute_within_minutes 2 "`dirname $0`/zway.sh GET_AUTH"
    exit -1
  fi
}

#
# This function verifies the auth cookie exists, and exits if it does not. If it
# exists, it is loaded in the variable AUTH_COOKIE
#
verifyAuthCookieOrExit() {
  if [ ! -e "${COOKIE_FILE}" ] ; then 
    log "error recalling auth cookie; file does not exist. Retrieving..."
    getAuthCookie
  fi

  AUTH_COOKIE=$(cat ${COOKIE_FILE})
  if [[ "$AUTH_COOKIE" == "" ]] ; then
    log "error empty auth cookie. Retrieving..."
    getAuthCookie
  fi

  AUTH_COOKIE=$(cat ${COOKIE_FILE})
}

#
# This function gets a list of supported devices from the ZWay controller
# The device list is stored in DEVICE_FILE in the format name:type:id
#  Requires a valid auth cookie
#
getDeviceList() {
  verifyAuthCookieOrExit
  DEVICE_LIST=$(curl -s -H "Cookie: ${AUTH_COOKIE}" http://${CONTROLLER_IP}:8083/ZAutomation/api/v1/devices | jq -r '.data.devices[] | "\(.metrics.title):\(.deviceType):\(.id)"')

  log "devices obtained"
  echo "$DEVICE_LIST" > $DEVICE_FILE 

  # trigger device manager to query state of the devices
  echo "" > ${GLUEOS_CONTROL_DIR}/device_query_zway
}

#
# This function locates a ZWay device by name and sets the DEVICE_ID variable
#  arg1 = device name
#
getDeviceID() {
  # find the device in the device list
  DEVICE=$(cat ${DEVICE_FILE} | grep -E "^$1:")
  
  if [ $? != 0 ] ; then
    ERR_STR="device <$1> does not exist in ZWay setup"
    log "error - $ERR_STR"
    alertError "ZWay Error" "$ERR_STR"
    exit -1
  fi

  # get device ID for query
  DEVICE_ID=$(echo "${DEVICE}" | cut -d':' -f3)
}

#
# This function queries the device state of a specific device
#  arg1 = device name
#
getDeviceState() {
  getDeviceID "$1"

  # query the device
  verifyAuthCookieOrExit
  DEVICE_STATE=$(curl -s -H "Cookie: ${AUTH_COOKIE}" http://${CONTROLLER_IP}:8083/ZAutomation/api/v1/devices/${DEVICE_ID} | jq -r '.data.metrics.level')

  echo "${DEVICE_STATE}"
}

#
# This function controls a specific binary device
#  arg1 = device name
#  arg2 = device state [on|off]
#
setBinaryDeviceState() {
  getDeviceID "$1"

  # control the device
  verifyAuthCookieOrExit
  CONTROL_CODE=$(curl -s -H "Cookie: ${AUTH_COOKIE}" http://${CONTROLLER_IP}:8083/ZAutomation/api/v1/devices/${DEVICE_ID}/command/$2 | jq -r '.code')

  if [ "${CONTROL_CODE}" == "401" ] ; then
    log "invalid authentication; attempting to retrieve new auth cookie"
    getAuthCookie
    log "re-attempting control of $1 to $2"
    setBinaryDeviceState "$1" "$2"
  else
    if [ "${CONTROL_CODE}" != "200" ] ; then
      ERR_STR="error controlling device: $CONTROL_CODE"
      log "$ERR_STR"
      alertError "ZWay Error" "$ERR_STR"
      exit -1
    fi
  fi
}

case $1 in
"GET_AUTH")
  REQ_NUM_ARGS=1
  verify_args && exit_if_verification_failed
  log "Getting auth cookie"
  getAuthCookie
  ;;
"GET_DEVICES")
  REQ_NUM_ARGS=1
  verify_args && exit_if_verification_failed
  log "Getting devices"
  getDeviceList
  ;;
"GET_DEVICE_STATE")
  REQ_NUM_ARGS=2
  verify_args && exit_if_verification_failed
  log "Getting device state for $2"
  getDeviceState "$2"
  ;;
"SET_DEVICE_STATE")
  REQ_NUM_ARGS=3
  verify_args && exit_if_verification_failed
  log "Setting device state for $2 to $3"
  setBinaryDeviceState "$2" "$3"
  ;;
*)
  log "unhandled command specified - $1"
  ;;
esac
