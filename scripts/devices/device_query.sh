#!/bin/bash
##
# This script queries devices updates their state files
#  arg1 = type of device (i.e. zway) or 'all'
#    - if arg1 is 'all', arg2 is ignored
#  arg2 = (optional) device name
#    - if arg2 is provided, will query that specific device
#    - if arg2 is not provided, will query all devices of that type
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_BINARIES=( )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit devices.sh

#
# script specific variables
#
TYPE=$1
DEVICE=""
if [[ $# == 2 ]] ; then
  DEVICE="$2"
fi

#
# This function queries zway devices
#
query_zway() {
  OLD_IFS=$IFS
  IFS=$'\n'
  for dev in ${DEVICES["ZWAY"]} ; do
    dname=$(echo "$dev" | cut -d',' -f1)
    droom=$(echo "$dev" | cut -d',' -f2)
    if [[ "$TYPE" == "all" ]] || [[ "$DEVICE" == "$dname" ]] || [[ -z "$DEVICE" ]] ; then
      dstate=$(`dirname $0`/zway.sh GET_DEVICE_STATE "$dname" | tail -n1)
      # only write valid states to state file
      if [[ "$dstate" == "off" ]] || [[ "$dstate" == "on" ]] ; then
        echo "$dstate" > "${GLUEOS_STATE_DIR}/room/$droom/device/$dname"
      else
        log "query_zway: Error; invalid state received for <$dname> in <$droom>: <$dstate>"
      fi
    fi
  done
}

#
# This function queries all devices
#
query_all()  {
  for dtype in ${DEVICE_TYPES[@]} ; do
    case $dtype in
    ZWAY|zway)
      query_zway
      ;;
    *)
      log "unsupported device query for type $type"
      ;;
    esac
  done
}

#
# This function sets up the state directory for all rooms which have devices.
# This is required to write state files for devices
#
setup_device_dirs() {
  for dtype in ${DEVICE_TYPES[@]} ; do
    OLD_IFS=$IFS
    IFS=$'\n'
    for dev in ${DEVICES["$dtype"]} ; do
      dname=$(echo "$dev" | cut -d',' -f1)
      droom=$(echo "$dev" | cut -d',' -f2)

      create_if_dne_room_state_dir "$droom"
    done
    IFS=$OLD_IFS
  done
}

setup_device_dirs
case $TYPE in 
all)
  log "querying all devices..."
  query_all
  ;;
ZWAY|zway)
  log "querying zway device/devices..."
  query_zway
  ;;
*)
  log "unhandled device type $TYPE"
  ;;
esac
