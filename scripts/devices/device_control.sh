#!/bin/bash
##
# This script controls devices
#  arg1 = name of device
#  arg2 = state [on|off]
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=2
REQ_BINARIES=( )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit devices.sh

#
# script specific variables
#
DEVICE=$1
STATE=$2

#
# This function iterates through the device types / devices and locates the
# type of device control was requested for, at which point it stores the 
# TYPE and ROOM for the device
#
get_device_type() {
  for dtype in ${DEVICE_TYPES[@]} ; do
    OLD_IFS=$IFS
    IFS=$'\n'
    for dev in ${DEVICES[$dtype]} ; do
      dname=$(echo "$dev" | cut -d',' -f1)
      droom=$(echo "$dev" | cut -d',' -f2)

      if [[ "$dname" == "$DEVICE" ]] ; then
        TYPE=$dtype
        ROOM="$droom"
        break
      fi
    done
    IFS=$OLD_IFS

    if [[ "$TYPE" != "unknown" ]] ; then
      break
    fi
  done
}

#
# This function wraps controlling ZWay devices
#
control_zway() {
  `dirname $0`/zway.sh SET_DEVICE_STATE "$DEVICE" "$STATE"
  SUCCESS=$?
}


#
# Main code to query device
#

# assume failure until query is successful
SUCCESS=-1

# perform query based on device type
get_device_type
case $TYPE in
ZWAY|zway)
  log "controlling zway device $DEVICE"
  control_zway
  ;;
*)
  log "unknown device type $TYPE for $DEVICE"
  ;;
esac

# if the query was successful, update the device state
if [[ $SUCCESS == 0 ]] ; then
  echo "$STATE" > "${GLUEOS_STATE_DIR}/room/${ROOM}/device/${DEVICE}"
fi

# regardless of whether the query was successful, schedule a query of the device
# in the next minute to ensure we have the correct device state
execute_within_minutes 1 "echo \"$DEVICE\" > ${GLUEOS_CONTROL_DIR}/device_query_$TYPE"
