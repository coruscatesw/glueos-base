# Third party configuration

glueos integrates with 3rd party applications to provide monitoring and control. Details about configuring the glueos integration with those 3rd party applications is included here for cases where configuration is required on the 3rd party application side.


## dnsmasq lease monitoring

dnsmasq can execute a script when updates are made to dns leases. glueos contains a script that can be executed and will send an alert via the configured glueos alerter if a new lease is added in a monitored range. To configure this on the glueos side, create ./config/dnsmasq.sh (see ./config/dnsmasq_sample.sh for reference). To configure this on the dnsmasq side, update the dnsmasq config file (often /etc/dnsmasq.d/<config>.conf) to include the following line:

dhcp-script=</path/to/>glueos-base/scripts/network/dnsmasq_lease_alert.sh


## SSH login monitoring

glueos contains a script that can be called when an SSH login is established and can send alerts based on ./config/ssh_login.sh. To configure the system to use the script, /etc/pam.d/sshd must be modifed. Add the following line to that file and restart the SSH daemon:

session optional pam_exec.so seteuid /home/<user>/glueos-base/scripts/network/ssh_login_notify.sh


## Local login monitoring

glueos contains a script than can be called when a local login is established. The script will send alerts based on ./config/local_login.sh. To configure the system to use the script, create a symbolic link to the script in /etc/profile.d/ (i.e. 'cd /etc/profile.d; sudo ln -s /home/myuser/glueos-base/scripts/local/login_notify.sh')

NOTE: The script requires the 'readlink' binary, which should be present by default on most linux installs. However, the invocation of readlink occurs before glueOS can verify dependencies for the script. Please verify readlink is installed prior to setting up.
