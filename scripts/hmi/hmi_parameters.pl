#!/usr/bin/perl

# This script takes 2 input strings, it compares them and outputs the percentage (using levenshtein distance)
# but also accounts for variable parameters.
#  arg1 = the value being read in from the hmi, eg. user input.
#  arg2 = the value from the translations, eg. what to compare against.
#
use File::Basename;
my $scriptDir = dirname($0);
my $MATCH_THRESHOLD = 90;

#
# Parse Parameter Conditions
#
sub MatchAndParse
{
  #Initial Declarations
  ($InputStr, $TransStr) = @_;
  $InputCpy = $InputStr;
  $StartWithParam = ($TransStr =~ m/^%/);
  $MatchPercent = 0;
  @ParamArray;
  @VarArray;

  #Build the Non-Parameter Array
  @NonParamArray = arraycleanup(split /%[^%]*%/, $TransStr);

  #Build the Parameter Array
  $ParsedStrRemSplit = $TransStr;
  foreach(@NonParamArray)
  {
    $ParsedStrRemSplit =~ s/$_//;
  }
  @ParamArray = arraycleanup(split /%/, $ParsedStrRemSplit);
  @InputSplitOnSpace = arraycleanup(split / /, $InputStr);

  foreach(@NonParamArray)
  {
    $InputSize = scalar(@InputSplitOnSpace);
    $TestSize = scalar(arraycleanup(split / /,$_));
    $BestMatch = 0;
    $BestIndex = 0;
    $StringToRemove = "";

    for($Index = 0;$Index < ($InputSize - $TestSize + 1); $Index++)
    {
      $TestString = trim($_);
      $Input = createsubstring($Index, $Index + $TestSize -1, @InputSplitOnSpace);
      $MatchResult = `$scriptDir/../general/fuzzy_match.pl "$TestString" "$Input"`;

      if($MatchResult > $BestMatch)
      {
        $BestMatch = $MatchResult;
        $BestIndex = $Index;
        $StringToRemove = createsubstring($BestIndex, $BestIndex + $TestSize -1, @InputSplitOnSpace);
      }

      if($BestMatch > $MATCH_THRESHOLD)
      {
        last;
      }
    }

    if($StartWithParam)
    {
      @tempArray = split( /$StringToRemove/, $InputStr);
      if (@tempArray[0] eq "")
      {
        #Immediate Fail
        print "0";
        exit;
      }
    }
    
    $InputCpy =~ s/$StringToRemove/|/;
    $InputCpy = trim($InputCpy);

    splice(@InputSplitOnSpace, 0, $BestIndex + scalar(split / /, $StringToRemove));
    push @MatchArray, $BestMatch;
  }

  foreach(@MatchArray)
  {
    $MatchTotal += $_;
  }

  $MatchPercent = $MatchTotal / (scalar(@MatchArray));

  #Cleanup the VarArray
  @VarArray = arraycleanup(split(/\|/, $InputCpy));
  
  #Build the Return Statement
  if(scalar @ParamArray eq scalar @VarArray)
  {
    $ArrayResults = "";
    $index = 0;
    foreach(@ParamArray)
    {
      $Parameter = trim(@ParamArray[$index]);
      $Variable = trim(@VarArray[$index]);
      if($index eq 0)
      {
        $ArrayResults = "%$Parameter%=$Variable";
      }
      else
      {
        $ArrayResults = "$ArrayResults,%$Parameter%=$Variable";
      }
      $index++;
    }
    print "$MatchPercent,$ArrayResults";
  }
  else
  {
    print "0";
  }
}

#Create SubString
sub createsubstring
{
  ($StartIndex, $EndIndex, @Array) = @_;
  @SubArray = @Array[$StartIndex .. $EndIndex];
  return trim(join(" ",@SubArray));
}


#Remove Blank Entries and Trim Entries
sub arraycleanup
{
  $index = 0;
  foreach(@_)
  {
    if(trim($_) eq '')
    {
      splice(@_, $index, 1);
    }
    $index++;
  }
  return @_;
}

#Trim Function (trims leading and ending whitespace)
sub trim
{
  ($string) = @_;
  $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string
}

#
# Main Code
#
#Parse Parameters
MatchAndParse($ARGV[0], $ARGV[1]);
