#!/bin/bash
##
# This script manages hmi inputs. It will parse the raw input to determine
# whether it matches any translation, and execute the corresponding action
# if one matches.
#  arg1 = raw HMI input
#
# Returns 0 if handled
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( perl bc )
#REQ_PACKAGES=( )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
declare -A TRANSLATION
include_config_or_exit hmi.sh

#
# script specific variables
#
HMI="$1"
BM_KEY=""
BM_VAL=""
BM_PER=0
SUCCESS=0

# This function loads the contents of a file into VALUE if the file exists,
# otherwise it sets SUCCESS to -1
#  arg1 = file, relative path from glueos-base/state directory
# 
get_value_if_exists () {
  FP="${GLUEOS_STATE_DIR}/$1"
  if [ -e "$FP" ] ; then
    VALUE=$(cat "$FP")
    log "value of <$1> is <$VALUE>"
  else
    log "error - file does not exist <$FP>"
    SUCCESS=-1
  fi
}


log "handling input <$HMI>"
declare -a VAR_ARGS
for tr in "${!TRANSLATION[@]}" ; do
  TR_KEY="$tr"
  OLD_IFS=$IFS
  IFS=$'\n'
  for TR_VAL in ${TRANSLATION[$tr]} ; do
    if [[ $TR_VAL =~ %*% ]]; then
      #Translation has parameter values, verify match.
      PARAM_ARRAY=$(${GLUEOS_SCRIPT_DIR}/hmi/hmi_parameters.pl "$1" "$TR_VAL")
      split_string_on_delimiter "$PARAM_ARRAY" ","
      if [ $(echo "${WORDS[0]} > $BM_PER" | bc) -eq 1 ] ; then
        VAR_ARGS=()
        BM_PER=${WORDS[0]}
        BM_KEY=$TR_KEY
        BM_VAL=$TR_VAL
        #Create the string of paraimeters to add to the script command
        INDEX=0
        for parameter in "${WORDS[@]}" ; do
          if [[ $parameter =~ %*% ]]; then
            PARAM=$(echo "$parameter"|sed 's/%//g')
            if [ $INDEX \> 2 ]; then
              PARAM=$PARAM" "
            fi
            VAR_ARGS=("${VAR_ARGS[@]}" "$PARAM")
          fi
          INDEX=$INDEX+1
        done
      fi
    else
      #   just use fuzzy logic compare of direct input and translations config
      MATCH_PERCENTAGE=$(${GLUEOS_SCRIPT_DIR}/general/fuzzy_match.pl "$1" "$TR_VAL")
      if [ $(echo "$MATCH_PERCENTAGE > $BM_PER" | bc) -eq 1 ] ; then
        VAR_ARGS=()
        BM_PER=$MATCH_PERCENTAGE
        BM_KEY=$TR_KEY
        BM_VAL=$TR_VAL
      fi
    fi
  done
  IFS=$OLD_IFS
done

log "best match ($BM_PER): [$BM_KEY] $BM_VAL"
if [ $(echo "$BM_PER > $MATCH_THRESHOLD" | bc) -eq 1 ] ; then
  case $BM_KEY in
  outside_temp_request)
    # grab the outside temp from the state file and send it as an alert
    get_value_if_exists "outside/temperature"
    if [ $SUCCESS == 0 ] ; then
      alertFeedback "Outside Temperature Report" "Currently $VALUE"
    fi
    ;;
  external_ip_request)
    # grab the external IP from the state file and send it as an alert
    get_value_if_exists "network/extip"
    if [ $SUCCESS == 0 ] ; then
      alertFeedback "External IP Report" "Current IP is $VALUE"
    fi
    ;;
  device_control_request)
    declare -A PARSED_VARS
    set_vars_from_input ${VAR_ARGS[@]}
    STATE=${PARSED_VARS["STATE"]}
    DEVICE=${PARSED_VARS["DEVICE"]}
    log "device_control_request: turning <$DEVICE> to <$STATE>"
    alertFeedback "HMI Manager" "Turning $STATE $DEVICE"
    control_device $DEVICE $STATE
    ;;
  wake_on_lan_request)
    declare -A PARSED_VARS
    set_vars_from_input ${VAR_ARGS[@]}
    COMPUTER=${PARSED_VARS["COMPUTER"]}
    log "wake_on_lan_request: WoL packet sent to $COMPUTER"
    alertFeedback "HMI Manager" "WoL packet sent to $COMPUTER"
    wake_on_lan $COMPUTER
    ;;
  renew_duckdns_request)
    log "renew_duckdns_request: Renewing Duck DNS"
    alertFeedback "HMI Manager" "Renewing Duck DNS"
    renew_duck_dns
    ;;
  temp_control_request)
    declare -A PARSED_VARS
    set_vars_from_input ${VAR_ARGS[@]}
    ROOM=${PARSED_VARS["ROOM"]}
    STATE=${PARSED_VARS["STATE"]}
    log "temp_control_request: setting temp control in <$ROOM> to <$STATE>"
    alertFeedback "HMI Manager" "Setting temperature control in $ROOM to $STATE"
    control_temp_management "$ROOM" "$STATE"
    ;;
  *)
    FP="${GLUEOS_SCRIPT_DIR}/custom/$BM_KEY"
    if [ -e "$FP" ] ; then
      if [ ${#VAR_ARGS[@]} -gt 0 ] ; then
        A_STR="executing custom script <$BM_KEY ${VAR_ARGS[@]}>"
        log "$A_STR"
        $FP ${VAR_ARGS[@]}
        alertInfo "HMI Manager" "$A_STR"
      else
        A_STR="executing custom script <$BM_KEY>"
        log "$A_STR"
        $FP
        alertInfo "HMI Manager" "$A_STR"
      fi
    else
      ERR_STR="unable to execute custom script <$BM_KEY>; file not found"
      log "$ERR_STR"
      alertError "HMI Manager Error" "$ERR_STR"
      SUCCESS=-1
    fi
    ;;
  esac
else
  ERR_STR="best match below threshold ($BM_PER < $MATCH_THRESHOLD)"
  log "$ERR_STR; unhandled"
  alertError "HMI Manager Error" "Unhandled input <$HMI>. $ERR_STR"
  SUCCESS=-1
fi

exit $SUCCESS
