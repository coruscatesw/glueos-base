#!/bin/bash
##
# This script should be sourced by other scripts to verify the necessary
# dependencies are in place prior to attempting to use them.
#  
#  args = none
#
#  environment variables
#   REQ_BINARIES = optional list of binary dependencies
#   REQ_PACKAGES = optional list of package dependencies
#   NUM_ARGS     = optional number of args of sourcing script (required if
#                  specifying REQ_NUM_ARGS)
#   REQ_NUM_ARGS = optional number of command line arguments required
##

DEPENDENCIES_VERIFIED=1

#
# if REQ_BINARIES is defined, ensure all binaries are present
#
verify_binaries() {
  if [[ -v REQ_BINARIES ]] ; then
    for bin in "${REQ_BINARIES[@]}"
    do
      exists=$(which ${bin})
      if [[ `which ${bin}` ]] ; then
        :
      else
        echo "Binary dependency $bin does not exist"
        DEPENDENCIES_VERIFIED=0
      fi
    done
  fi
}

#
# if REQ_PACKAGES is defined, ensure all packages are present
#
verify_packages() {
  if [[ -v REQ_PACKAGES ]] ; then
    for pkg in "${REQ_PACKAGES[@]}" ; do
      exists=$(dpkg-query -l ${pkg})
      if [ $? -ne 0 ] ; then
        echo "Package dependency $pkg not met"
        DEPENDENCIES_VERIFIED=0
      fi
    done
  fi
}

#
# if REQ_NUM_ARGS is defined, ensure correct number of command line args
#
verify_args() {
  if [[ -v REQ_NUM_ARGS ]] ; then
    if [ $NUM_ARGS != ${REQ_NUM_ARGS} ] ; then
      echo "Required number of arguments not met"
      DEPENDENCIES_VERIFIED=0
    fi
  fi
}

#
# if verification fails, print an error and exit
#
exit_if_verification_failed() {
  if [ $DEPENDENCIES_VERIFIED != 1 ] ; then
    echo "Dependencies for `basename $0` not verified; exiting"
    exit -1
  fi
}


verify_binaries
verify_packages
verify_args
exit_if_verification_failed
