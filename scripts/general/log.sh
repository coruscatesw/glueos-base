#!/bin/bash
##
# This script facilitates logging. It depends on variables set in globals.sh
# thus cannot be used independently
#
##

# This function wraps log output to provide a consistent and helpful scheme
#  arg1 = log entry
#  arg2 = log file [optional]. If not specified, logs to std out
log() {
  TSTAMP=`date "+%Y%m%d %H:%M:%S"`
  if [[ $# == 2 ]] ; then  
    echo "[$TSTAMP] `basename $0`: $1" >> "$2"
  else
    echo "[$TSTAMP] `basename $0`: $1"
  fi
}

# Convenience method to log config
logConfig() {
  log "$1" "/var/log/glueos/config.log"
}

# Convenience method to log control
logControl() {
  log "$1" "/var/log/glueos/control.log"
}

# Convenience method to log state
logState() {
  log "$1" "/var/log/glueos/state.log"
}
