#!/bin/bash
##
# This script wraps functionality that wraps alerts. It depends on variables
# set in globals.sh, thus cannot be used independently
#
# Note on alert types (also in the alerts_sample.sh config):
#  feedback - alerts sent in response to a user request
#  security - alerts sent on specific triggers, i.e. along the lines of security
#  info - informational alerts sent about glueos operation, most likely used 
#         for debug
#  error - error alerts sent about glueos operation, sent when scripts or 
##

#
# This function wraps sending alerts
#  arg1 = alert type, i.e. info|warning|error
#  arg2 = alert title
#  arg3 = alert text
alert() {
  if [[ $# != 3 ]] ; then 
    echo "alert: invalid number of arguments specified, cannot sent alert"
  fi
  AF="${GLUEOS_CONTROL_DIR}/alert_`uuidgen`"
  echo "$1" > "$AF"
  echo "$2" >> "$AF"
  echo "$3" >> "$AF"
}

# This function wraps sending feedback alerts
#  arg1 = alert title
#  arg2 = alert text
alertFeedback() {
  if [[ $# != 2 ]] ; then 
    echo "alertFeedback: invalid number of arguments specified, cannot sent alert"
  fi
  alert "feedback" "$1" "$2"
}

# This function wraps sending info alerts
#  arg1 = alert title
#  arg2 = alert text
alertInfo() {
  if [[ $# != 2 ]] ; then 
    echo "alertInfo: invalid number of arguments specified, cannot sent alert"
  fi
  alert "info" "$1" "$2"
}

# This function wraps sending warning alerts
#  arg1 = alert title
#  arg2 = alert text
alertSecurity() {
  if [[ $# != 2 ]] ; then 
    echo "alertSecurity: invalid number of arguments specified, cannot sent alert"
  fi
  alert "security" "$1" "$2"
}

# This function wraps sending error alerts
#  arg1 = alert title
#  arg2 = alert text
alertError() {
  if [[ $# != 2 ]] ; then 
    echo "alertError: invalid number of arguments specified, cannot sent alert"
  fi
  alert "error" "$1" "$2"
}
