#!/bin/bash

##
# This script is used by several other scripts
#  arg1 = glueos-base relative directory
#
# List of functions (for convenience)
#  alerts.sh
#    alert
#    alertInfo
#    alertWarning
#    alertError
#  log.sh
#    log
#    logConfig
#    logControl
#    logState
#  control_device
#  control_temp_management
#  create_if_dne_room_state_dir
#  execute_within_minutes
#  get_glueos_relative_pathname
#  get_room_from_filename
#  include_config_or_exit
#  renew_duck_dns
#  sanitize_string_for_remote_command
#  set_host_state
#  set_vars_from_input
#  split_string_on_delimiter
#  wake_on_lan
##

#
# verify all dependencies
#
OLD_NUM_ARGS=$NUM_ARGS
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( uuidgen at sed )
REQ_PACKAGES=( uuid-runtime )
verify_args && verify_binaries && exit_if_verification_failed
NUM_ARGS=$OLD_NUM_ARGS

#
# Global variables / functions used by other scripts
#
GLUEOS_BASE_DIR=$1
GLUEOS_CONFIG_DIR=${GLUEOS_BASE_DIR}/config
GLUEOS_CONTROL_DIR=${GLUEOS_BASE_DIR}/control
GLUEOS_SCRIPT_DIR=${GLUEOS_BASE_DIR}/scripts
GLUEOS_STATE_DIR=${GLUEOS_BASE_DIR}/state

# include the alert functions
. ${GLUEOS_SCRIPT_DIR}/general/alerts.sh

# include the log functions
. ${GLUEOS_SCRIPT_DIR}/general/log.sh

#
# This method wraps controlling devices
#  arg1 = device name
#  arg2 = requested state [on|off]
#
control_device() {
  DEVICE_CONTROL_FILENAME=device_`uuidgen`
  echo "$1" > "${GLUEOS_CONTROL_DIR}/${DEVICE_CONTROL_FILENAME}"
  echo "$2" >> "${GLUEOS_CONTROL_DIR}/${DEVICE_CONTROL_FILENAME}"
}

#
# This method wraps controlling temp management
#  arg1 = room name
#  arg2 = requested state [active|inactive]
#
control_temp_management() {
  TEMP_CONTROL_FILENAME=temp_`uuidgen`
  echo "$1" > "${GLUEOS_CONTROL_DIR}/${TEMP_CONTROL_FILENAME}"
  echo "$2" >> "${GLUEOS_CONTROL_DIR}/${TEMP_CONTROL_FILENAME}"
}

#
# This method wraps sending a WoL packet
#  arg1 = hostname to send WoL
#
wake_on_lan() {
  WOL_CONTROL_FILENAME=wol_`uuidgen`
  echo "$1" > "${GLUEOS_CONTROL_DIR}/${WOL_CONTROL_FILENAME}"
}

#
# This method wraps renewing duck dns
#
renew_duck_dns() {
  touch "${GLUEOS_CONTROL_DIR}/duckdns_renewal_`uuidgen`"
}

#
# This method attempts to source the config file specified from the config dir.
# If the file does not exist, the script will exit
#  arg1 = config file (i.e. 'alerts.sh')
#
include_config_or_exit () {
  CONFIG_FILE=${GLUEOS_CONFIG_DIR}/$1

  if [ -e "$CONFIG_FILE" ] ; then
    . $CONFIG_FILE
  else
    log "config file <$1> does not exist; exiting"
    exit -1
  fi
}

#
# This function executes a command before the number of minutes specified have 
# passed. The command will execute between X-1 and X minutes
#  arg1 = minutes in the future
#  arg2 = command
#
execute_within_minutes () {
  ts=$(uuidgen)
  qfile="/tmp/atq_${ts}"
  printf "$2" > $qfile
  at -f $qfile now + $1 minutes >> /dev/null 2>&1
  rm $qfile
}

#
# This function splits a string on a designated delimiter and populates the array
# WORDS with the split string
#  arg1 = string to split (i.e. 'split,this,up please')
#  arg2 = delimitir (i.e. ',')
#
split_string_on_delimiter() {
  STR="$1"
  DEL="$2"
  OLD_IFS=$IFS
  IFS="$DEL"
  read -ra WORDS <<< "$STR"
  IFS=$OLD_IFS
}

#
# This function gets the name of a room from a file name. It sets the ROOM
# variable if a room was found.
#  arg1 = filename
#
get_room_from_filename() {
  split_string_on_delimiter "$1" "/"
  for word in "${WORDS[@]}" ; do
    if [[ $SET_ROOM_NEXT == 0 ]] ; then
      ROOM="$word"
      break
    fi
    if [[ "$word" == "room" ]] ; then
      SET_ROOM_NEXT=0
    fi
  done
}

#
# This function takes in a filename and sets RELATIVE_PATHNAME to the glues-base
# relative path. I.e. input "/home/myuser/glueos-base/state/network/extip" will
# set RELATIVE_PATHNAME to "state/network/extip"
#  arg1 = full pathname
#
get_glueos_relative_pathname() {
  RELATIVE_PATHNAME=""
  CONCAT=-1
  split_string_on_delimiter "$1" "/"
  for word in "${WORDS[@]}" ; do
    if [[ $CONCAT == 0 ]] ; then
      RELATIVE_PATHNAME="${RELATIVE_PATHNAME}/${word}"
    else
      if [[ "$word" == "glueos-base" ]] ; then
        CONCAT=0
      fi
    fi
  done
  RELATIVE_PATHNAME="${RELATIVE_PATHNAME:1}"
}

#
# This function takes in a string and sets SANITIZED_STRING to the sanitized 
# version to be used in remote commands. It will convert all spaces to be 
# escaped
#
sanitize_string_for_remote_command() {
  INPUT="$1"
  SANITIZED_STRING="$(echo "$INPUT" | sed 's/ /\\ /g')"
}

#
# This function takes 1..n input arguments of the form VAR_NAME=value and sets
# the variables in an array PARSED_VARS.
#  arg1 = input (VAR=value)
#  ...
#  argN = input (VAR=value)
# 
# NOTE: 'declare -A PARSED_VARS' absolutely MUST be called from the calling
# script prior to invoking this method. Otherwise, it will not work.
#
set_vars_from_input() {
  for var in "$@" ; do
    split_string_on_delimiter "$var" "="
    if [[ ${#WORDS[@]} > 0 ]] ; then
      PARSED_VARS["${WORDS[0]}"]="${WORDS[@]:1}"
    fi
  done
}

#
# This function facilitates updating a host state file
#  arg1 = hostname
#  arg2 = status <active|inactive>
#
set_host_state() {
  echo "$2" > "${GLUEOS_STATE_DIR}/network/hosts/$1"
}

#
# This function wraps creating a state room dir
#  arg1 = room name
# 
create_if_dne_room_state_dir() {
  DIR="${GLUEOS_STATE_DIR}/room/${1}/device"
  if [ ! -d "$DIR" ] ; then
    mkdir -p "$DIR"
    log "initializing room state directory for <$1>"
  fi
}
