#!/usr/bin/perl
use List::Util qw(min);
#
# This script takes 2 input strings, compares them, and outputs the percentage
# match
#  arg1 = first string to compare
#  arg2 = second string to compare
#


#
# perl implementation of the levenshtein distance (between 2 strings) algorithm
#
sub levenshtein
{
    my ($str1, $str2) = @_;
    my @sp1 = split //, lc($str1);
    my @sp2 = split //, lc($str2);

    my @dist;
    $dist[$_][0] = $_ foreach (0 .. @sp1);
    $dist[0][$_] = $_ foreach (0 .. @sp2);

    foreach my $i (1 .. @sp1)
    {
      foreach my $j (1 .. @sp2)
      {
        my $cost = $sp1[$i-1] eq $sp2[$j-1] ? 0 : 1;
        $dist[$i][$j] = min($dist[$i-1][$j]+1, 
                            $dist[$i][$j-1]+1, 
                            $dist[$i-1][$j-1]+$cost);
      }
    }

    return $dist[@sp1][@sp2];
}

#
# main code
#
my $str1=$ARGV[0];
my $str2=$ARGV[1];

my $lstr1=length($str1);
my $lstr2=length($str2);

my $max_len=$lstr1 >= $lstr2 ? $lstr1 : $lstr2;
my $lev=levenshtein($str1, $str2);

my $percentage_match=100-($lev/$max_len*100);
print "$percentage_match\n";
