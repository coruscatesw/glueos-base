#!/usr/bin/python
# This script is written specifically for the DHT22 temp sensor connected to
# a Raspberry Pi. 
# 
# Due to the Adafruit implementation, this script is required to be executed
# as root (for access to the GPIO pins). It should be added to root's crontab 
# for periodic execution.
# > sudo crontab -e
# */2 * * * * /home/pi/dht22-pi.py (executes every 2 minutes)
#
# This script writes the temperature & humidity gathered from the sensor to
# the state/<room> directory

import Adafruit_DHT
import sys, os

sys.path.append(os.path.join(os.path.dirname(__file__),"../../config"))
from dht22 import *

# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

# Debugging output
#print "C: "+str(temperature)

# temp is reported in C; convert to F
tempF = (temperature*1.8)+32

outdir = "./../state/room/"+room
# Ensure output directory exists
try:
  os.makedirs(outdir)
except OSError:
  if not os.path.isdir(outdir):
    raise

# Note that sometimes you won't get a reading and the results will be null 
# because Linux can't guarantee the timing of calls to read the sensor
# If this happens, don't report the temp
if temperature is not None:
  #print "Temp: "+str(tempF)
  tfile = outdir+"/temperature"
  fo = open(tfile, "w")
  fo.write(str(tempF))
  fo.close()
  os.chmod(tfile,0o777)
if humidity is not None:
  #print "Humidity: "+str(humidity)
  hfile = outdir+"/humidity"
  fo = open(hfile, "w")
  fo.write(str(humidity))
  fo.close()
  os.chmod(hfile,0o777)
