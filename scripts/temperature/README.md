# References

To incorporate a USB Temper temp sensor, see the following repository. Set the process to run (as root) in crontab and periodically (~3 minutes) write the temp to a state file in glueos-base/state/room/<room_name>/temperature.

https://github.com/shakemid/pcsensor-temper
