#!/bin/bash
##
# This script manages the temperature
#  arg1 = room name
#  arg2 = [temperature|humidity]
#  arg3 = value of arg2
#
# Returns 0
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=3
REQ_BINARIES=( cat )
REQ_PACKAGES=( )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit temp_management.sh

#
# script specific variables
#
ROOM="$1"
TH="$2"
VAL="$3"

#
# This function loads the paramaters for the specified room into
#  HEAT_DEVICES, AC_DEVICES, ACTIVE_TEMP_RANGE, INACTIVE_TEMP_RANGE, and VARIANCE
# It also sets the following convenience variables
#  ACTIVE, CONTROL_HEAT, CONTROL_AC
#  HIGH, LOW (both according to active state)
#
load_room_params() {
  HEAT_DEVICES=${HEAT_DEVICES["$ROOM"]}
  AC_DEVICES=${AC_DEVICES["$ROOM"]}
  ACTIVE_TEMP_RANGE=${ACTIVE_TEMP_RANGE["$ROOM"]}
  INACTIVE_TEMP_RANGE=${INACTIVE_TEMP_RANGE["$ROOM"]}
  VARIANCE=${VARIANCE["$ROOM"]}

  TCF="${GLUEOS_STATE_DIR}/room/${ROOM}/temp_control"
  create_if_dne_room_state_dir "$ROOM"
  if [ ! -e "$TCF" ] ; then
    log "control file missing; initializing control for $ROOM to inactive"
    echo "inactive" > "$TCF"
  fi
  NULL_CMD=$(cat "$TCF" | grep -E "^active")
  ACTIVE=$?
  if [[ $ACTIVE == 0 ]] ; then
    HIGH=$(echo "$ACTIVE_TEMP_RANGE" | cut -d'-' -f2)
    LOW=$(echo "$ACTIVE_TEMP_RANGE" | cut -d'-' -f1)
  else
    HIGH=$(echo "$INACTIVE_TEMP_RANGE" | cut -d'-' -f2)
    LOW=$(echo "$INACTIVE_TEMP_RANGE" | cut -d'-' -f1)
  fi

  # by default, assume we are controlling both heat and ac
  CONTROL_HEAT=0
  CONTROL_AC=0
  if [[ -z "$HEAT_DEVICES" ]] ; then
    CONTROL_HEAT=-1
  fi
  if [[ -z "$AC_DEVICES" ]] ; then
    CONTROL_AC=-1
  fi

  log "load_room_params[$ROOM] - ${LOW}[${CONTROL_HEAT}]-${HIGH}[${CONTROL_AC}]"
}

#
# This function wraps controlling heat/ac devices
#  arg1 = [heat|ac]
#  arg2 = [on|off]
#
control_devices () {
  DEVICES=$HEAT_DEVICES
  if [[ "$1" == "ac" ]] ; then
    DEVICES=$AC_DEVICES
  fi

  OLD_IFS=$IFS
  IFS=","
  for dev in $DEVICES ; do
    log "turning $2 $dev"
    control_device "$dev" $2
  done
  IFS=$OLD_IFS
}

#
# 
#
handle_inside_humidity_update() {
  HUM=${VAL%?}
  #@TODO
}

#
#
#
handle_outside_humidity_update() {
  HUM=${VAL%?}
  #@TODO
}

#
# 
#
handle_inside_temp_update() {
  TEMP=${VAL%?}
  log "handle_inside_temp_update: $ROOM temp is $TEMP"

  if (( $TEMP < ( $LOW - $VARIANCE ) )) && [[ $CONTROL_HEAT == 0 ]] ; then
    # it's cold, turn the heat on
    control_devices heat on
  fi
  if (( $TEMP > ( $HIGH + $VARIANCE ) )) && [[ $CONTROL_AC == 0 ]] ; then
    # it's hot, turn the AC on
    control_devices ac on
  fi
  if (( $TEMP > ( $LOW + $VARIANCE ) )) && [[ $CONTROL_HEAT == 0 ]] ; then
    control_devices heat off
  fi
  if (( $TEMP < ( $HIGH - $VARIANCE ) )) && [[ $CONTROL_AC == 0 ]] ; then
    control_devices ac off
  fi
}

#
# 
#
handle_outside_temp_update() {
  TEMP=${VAL%?}
  #@TODO
}


#
# Main control logic
#
case $ROOM in
outside)
  case $TH in
  temperature)
    handle_outside_temp_update
    ;;
  humidity)
    handle_outside_humidity_update 
    ;;
  *)
    log "error - unspecified update type of $TH"
    exit -1
    ;;
  esac
  ;;
*)
  APPLICABLE=-1
  split_string_on_delimiter "$ROOMS_CONTROLLED" ","
  for room in "${WORDS[@]}" ; do
    if [[ "$room" == "$ROOM" ]] ; then
      APPLICABLE=0
      break
    fi
  done
  if [[ $APPLICABLE != 0 ]] ; then
    log "ignoring temp update for <$ROOM>; room not controlled"
    exit
  fi
  load_room_params

  case $TH in
  temperature)
    handle_inside_temp_update
    ;;
  humidity)
    handle_inside_humidity_update 
    ;;
  *)
    log "error - unspecified update type of $TH"
    exit -1
    ;;
  esac
  ;;
esac

