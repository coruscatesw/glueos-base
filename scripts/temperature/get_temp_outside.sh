#!/bin/bash
##
# This script gets the outside temp
#
# Returns 0
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
REQ_BINARIES=( weather grep awk)
REQ_PACKAGES=( weather-util )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
include_config_or_exit temp_outside.sh

#
# script specific variables
#
EXT_TEMP_FILE=${GLUEOS_STATE_DIR}/outside/temperature
EXT_HUM_FILE=${GLUEOS_STATE_DIR}/outside/humidity

#
# get outside temp
#
otemp=$(weather $ZIP | grep Temperature | awk '{print $2 $3}')
ohum=$(weather $ZIP | grep Humidity | awk '{print $3}')

echo "$otemp" > "$EXT_TEMP_FILE"
echo "$ohum" > "$EXT_HUM_FILE"
