#!/bin/bash
##
# This is a skeleton script that can be use to rapidly create new scripts for
# use with GlueOS
#
# Returns 0 if successful
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
#REQ_NUM_ARGS=2   # uncomment to enforce the number of command line args
#REQ_BINARIES=( ) # uncomment to require specific binaries for execution
#REQ_PACKAGES=( ) # uncomment to require specific packages for execution
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
#include_config_or_exit myconf.sh # uncomment to include a config file (or exit)

#
# script specific variables
#
SUCCESS=0

#
# main script code
#

exit $SUCCESS
