#!/bin/bash
##
# This script interfaces with foscamH264 cameras
#  arg1 = command (GET_SNAPSHOT)
#  arg2 = room
#  arg3 = host
#  arg4 = user
#  arg5 = password
#
# Returns 0 if successful
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=5
REQ_BINARIES=( curl )
#REQ_PACKAGES=( ) # uncomment to require specific packages for execution
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# script specific variables
#
SUCCESS=0

#
# This method determines whether the camera is reachable
#  arg1 = host name / IP
is_available() {
#echo "is_available passed $@"
  AVAIL=$(ping -c1 -w1 $1)
  RV=$?
  if [[ $RV != 0 ]] ; then
    log "host <$1> is not available"
  fi
  return $RV
}

#
# This method gets a screenshot from the camera
#  arg1 = room
#  arg2 = host name / IP
#  arg3 = user
#  arg4 = password
get_screenshot() {
  IMG_URL=$(curl -G -s --data-urlencode "usr=${3}" --data-urlencode "pwd=${4}" "http://${2}/cgi-bin/CGIProxy.fcgi?cmd=snapPicture")
  if [[ $? != 0 ]] ; then
    log "unable to retrieve image URL from <$2>: $IMG_URL"
    return -1
  fi
  # IMG_URL is in the format '<html><body><img src=\"../snapPic/Snap_20180219-135119.jpg\"/></body></html>'
  IMG_SRC=$(echo "$IMG_URL" | sed 's/.*img src="//' | sed 's/.jpg".*/.jpg/')

  NULL_CMD=$(curl -s "http://${2}/${IMG_SRC}" -o ${GLUEOS_STATE_DIR}/room/${1}/snapshot.jpg)
  if [[ $? != 0 ]] ; then
    log "unable to save image: $?"
    return -1
  fi
}

#
# main script code
#
case $1 in
GET_SNAPSHOT)
  $(is_available "$3") && $(get_screenshot "$2" "$3" "$4" "$5")
  ;;
*)
  log "unhandled command request: $1"
  ;;
esac

exit $SUCCESS
