#!/bin/bash
##
# This script encapsulates camera control
#  arg1 = command (GET_SNAPSHOT)
#  arg2 = room
#
# Returns 0 if successful
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=2
#REQ_BINARIES=( ) # uncomment to require specific binaries for execution
#REQ_PACKAGES=( ) # uncomment to require specific packages for execution
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
declare -A CAMERA_TYPE
declare -A CAMERA_IP
declare -A CAMERA_USER
declare -A CAMERA_PASSWORD
include_config_or_exit cameras.sh

#
# script specific variables
#
SUCCESS=0
ROOM="$2"

get_snapshot() {
  if [ ${CAMERA_IP["$ROOM"]+exists} ] ; then
    T=${CAMERA_TYPE["$ROOM"]}
    H=${CAMERA_IP["$ROOM"]}
    U=${CAMERA_USER["$ROOM"]}
    P=${CAMERA_PASSWORD["$ROOM"]}
    
    create_if_dne_room_state_dir "$ROOM"

    case $T in
    FoscamH264)
      `dirname $0`/foscamh264.sh "GET_SNAPSHOT" "$ROOM" "$H" "$U" "$P"
      ;;
    *)
      log "unknown camera type $T for $ROOM"
      ;;
    esac
  fi
}

#
# main script code
#
case $1 in
GET_SNAPSHOT)
  get_snapshot
  ;;
*)
  log "unknown camera control"
  ;;
esac
exit $SUCCESS
