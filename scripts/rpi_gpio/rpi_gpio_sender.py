#!/usr/bin/python

import socket
import sys

MY_SERVER_ADDRESS = ("127.0.0.1", 14666)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(MY_SERVER_ADDRESS)

args = sys.argv
args.pop(0)

message="GPIO " + " ".join(args)

print "Message:  <" + message + ">"

sock.sendall(message)
