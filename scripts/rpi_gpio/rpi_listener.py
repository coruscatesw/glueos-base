#!/usr/bin/python

import socket
import RPi.GPIO as GPIO

MY_SERVER_ADDRESS = ("127.0.0.1", 14666)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(MY_SERVER_ADDRESS)
sock.listen(100)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#Boardlist hasn't been tested.
BoardList = {"0":"GPIO.BOARD",
             "1":"GPIO.BCM",
             "2":"None"}

#Types don't seem to lineup correctly.
TypeList = {"0":"GPIO.OUT",
            "1":"GPIO.IN",
            "2":"GPIO.SPI",
            "3":"GPIO.I2C",
            "4":"GPIO.HARD_PWM",
            "5":"GPIO.SERIAL",
            "6":"GPIO.UNKNOWN"}

#In the PinList there will be a object (Pin #, PinType)
PinList = {}
PWMList = {}

def TestPinAvail(Pin):
  if int(Pin) > 0:
    if PinList.get(Pin):
      print "Pin #" + str(Pin) + " already in use as " + str(PinList.get(Pin))
      return 0
    else:
      return 1

def GetPinType(Pin):
  if int(Pin) > 0:
    print TypeList[str(GPIO.gpio_function(int(Pin)))]

def PinSetup(Pin, Type):
  if int(Pin) > 0:
    PinType = GPIO.IN
    if Type.upper() == "OUT" or Type.upper() == "HIGH":
      PinType = GPIO.OUT
    GPIO.setup(int(Pin), PinType)
    PinList[Pin] = Type.upper()
    print "Setting up #" + str(Pin) + " as an " + str(Type.upper()) + " pin."
  else:
    print "Invalid Pin"

def PinRemove(Pin):
  if int(Pin) > 0:
    GPIO.cleanup(int(Pin))
    del PinList[Pin]
    print "Removing the #" + str(Pin) + " GPIO pin."
  else:
    print "Invalid Pin"

def PinSetVal(Pin, NewVal):
  if int(Pin) > 0:
    if PinList[Pin] == "OUT" or PinList[Pin] == "HIGH":
      GPIO.output(int(Pin), int(NewVal))
      print "Updating the value of #" + str(Pin) + " pin to " + str(NewVal)
    else:
      print "Pin is not in output mode"
  else:
    print "Invalid Pin"

def PinSetDir(Pin, Direction):
  PinRemove(Pin)
  PinSetup(Pin, Direction)

def PWMSetup(Pin, Frequency):
  if int(Pin) > 0:
    PinSetup(Pin, "OUT")
    PWMobj = GPIO.PWM(int(Pin), float(Frequency))
    PWMList[Pin] = PWMobj
    PinList[Pin] = "PWM"
    PWMobj.start(1)
    print "Setting up #" + str(Pin) + " as a PWM pin."
  else:
    print "Invalid Pin"

def PWMRemove(Pin):
  if int(Pin) > 0:
    PWMobj = PWMList.get(Pin)
    if PWMobj:
      PWMobj.stop()
      del PinList[Pin]
      del PWMList[Pin]
      print "Removing the #" + str(Pin) + " PWM pin."
  else:
    print "Invalid Pin"

def PWMChangeFreq(Pin, Frequency):
  PWMobj = PWMList.get(Pin)
  if PWMobj:
    PWMobj.ChangeFrequency(float(Frequency))
    print "Updating the frequency of #" + str(Pin) + " pin to " + str(Frequency)
  else:
    print "PWM Object invalid."

def PWMChangeDC(Pin, NewDC):
  PWMobj = PWMList.get(Pin)
  if PWMobj:
    if float(NewDC) >= 0.0 and float(NewDC) <= 100.0:
      PWMobj.ChangeDutyCycle(float(NewDC))
      print "Updating the duty cycle of #" + str(Pin) + " pin to " + str(NewDC)
    else:
      print "Duty Cycle of " + str(NewDC) + " not between (0.0 - 100.0)"
  else:
    print "PWM Object invalid."

#Main Function
try:
  while True:
    connection, client_address = sock.accept()
    try:
      data = connection.recv(1024)
      result = data.split(" ")

      #Initialization
      GPIO_or_PWM = ""
      Command = ""
      Pin = "-1"
      Value = 0

      if len(result) >= 3:
        GPIO_or_PWM = result[0]
        Command = result[1]
        Pin = result[2]
        Value = 0

      if len(result) == 4:
        Value = result[3]

      #Test Conditions
      #GPIO Only
      if (GPIO_or_PWM == "GPIO"):
        if Command == "EXPORT":
          if TestPinAvail(Pin):
            PinSetup(Pin, Value)
        elif Command == "UNEXPORT":
          PinRemove(Pin)
        elif Command == "GET_TYPE":
          GetPinType(Pin)
        elif Command == "GET_ACTIVE_LOW":
          print "TODO"
        elif Command == "SET_ACTIVE_LOW":
          print "TODO"
        elif Command == "GET_DIRECTION":
          print "GET_DIRECTION"
          print str(PinList[Pin])
        elif Command == "SET_DIRECTION":
          PinSetDir(Pin, Value)
        elif Command == "GET_EDGE":
          print "TODO"
        elif Command == "SET_EDGE":
          print "TODO"
        elif Command == "GET_VALUE":
          print GPIO.input(int(Pin))
        elif Command == "SET_VALUE":
          PinSetVal(Pin, Value)
        else:
          print "Unknown " + GPIO_or_PWM + "command <" + str(Command) + ">"
      #PWM Only
      elif (GPIO_or_PWM == "PWM"):
        if Command == "EXPORT":
          if TestPinAvail(Pin):
            PWMSetup(Pin, Value)
        elif Command == "UNEXPORT":
          PWMRemove(Pin)
        elif Command == "GET_TYPE":
          GetPinType(Pin)
        elif Command == "GET_DUTY_CYCLE":
          print "TODO"
        elif Command == "SET_DUTY_CYCLE":
          PWMChangeDC(Pin, Value)
        elif Command == "GET_ENABLE":
          print "TODO"
        elif Command == "SET_ENABLE":
          print "TODO"
        elif Command == "GET_PERIOD":
          print "TODO"
        elif Command == "SET_PERIOD":
          PWMChangeFreq(Pin, Value)
        elif Command == "GET_POLARITY":
          print "TODO"
        elif Command == "SET_POLARITY":
          print "TODO"
        else:
          print "Unknown " + GPIO_or_PWM + "command <" + str(Command) + ">"
      else:
        print "Unknown GPIO_or_PWM <" + str(GPIO_or_PWM) + ">"
    finally:
      connection.close()
finally:
  for key in PWMList.keys():
    PWMRemove(key)
  GPIO.cleanup()
  connection.close()
