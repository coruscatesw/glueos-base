#!/usr/bin/python

import os
import subprocess
import time

cwd=os.getcwd()
filepath = cwd + "/rpi_pwm_sender.py"

Export = [filepath, "EXPORT",  "21", "50"]
DutyCycle = [filepath, "SET_DUTY_CYCLE",  "21"]
Unexport = [filepath, "UNEXPORT",  "21"]

subprocess.call(Export)

try:
  while 1:
    for dc in range(0, 101, 25):
      DutyCycle.append(str(float(dc)))
      subprocess.call(DutyCycle)
      DutyCycle.pop()
      time.sleep(0.1)
    for dc in range(100, -1, -25):
      DutyCycle.append(str(float(dc)))
      subprocess.call(DutyCycle)
      DutyCycle.pop()
      time.sleep(0.1)
except KeyboardInterrupt:
  pass

subprocess.call(Unexport)
