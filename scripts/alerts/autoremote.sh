#!/bin/bash
##
# This script sends notifications to autoremote (Android app)
#  arg1 = title of notification
#  arg2 = message in notification
#
# Returns 0 on success
#
# Reference: https://joaoapps.com/autoremote/linux/
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=2
REQ_BINARIES=( curl )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
declare -A AR_KEY
include_config_or_exit autoremote.sh

#
# send notification
#
SUCCESS=0
for k in "${!AR_KEY[@]}" ; do
  AR_KEY="${AR_KEY[$k]}"
  this_success=$(curl -s --data-urlencode "title=$1" --data-urlencode "text=$2" http://autoremotejoaomgcd.appspot.com/sendnotification?key=${AR_KEY})
  if [[ SUCCESS == 0 ]] ; then
    SUCCESS=`echo "$this_success" | grep -q OK`
  fi
done

exit $SUCCESS
