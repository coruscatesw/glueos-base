#!/bin/bash
##
# This script sends notifications to ifttt via webhooks
#  arg1 = title of notification
#  arg2 = message in notification
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=2
REQ_BINARIES=( curl )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

title=$1
message=`echo $2`

#
# include configuration file
#
include_config_or_exit ifttt-text.sh

#
# send notification
#
SUPPRESS_ME=$(curl -s -X POST -H "Content-Type: application/json" -d "{\"value1\":\"${title}\",\"value2\":\"${message}\"}" "https://maker.ifttt.com/trigger/${EVENT}/with/key/${MAKER_KEY}")

SUCCESS=$?

exit $SUCCESS
