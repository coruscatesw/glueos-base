#!/bin/bash
##
# This script sends notifications via the configured alerter
#  arg1 = type of alert
#  arg2 = title of alert
#  arg3 = message in alert
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=3
#REQ_BINARIES=( )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file that contains alerter
#
declare -A show_alerts
include_config_or_exit alerts.sh

#
# send alert
#
if [[ "${show_alerts[$1]}" == "yes" ]] ; then 
  TS=`date "+%H:%M:%S on %Y.%m.%d"`
  HOST=`hostname`
  MESSAGE="${3}"$'\nFrom '"$HOST @ $TS"
  for alerter in ${ALERTER[@]} ; do
    `dirname $0`/${alerter} "$2" "$MESSAGE"
  done
else
  log "not sending alert type <$1>"
fi
