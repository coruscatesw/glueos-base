#!/bin/bash
##
# This script sends notifications to Join (Android app, Chrome extension)
#  arg1 = title of notification
#  arg2 = message in notification
#
# Returns 0 on success
#
# Reference: https://joinjoaomgcd.appspot.com/
#            https://joaoapps.com/join/api/
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=2
REQ_BINARIES=( curl )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file
#
declare -A DEVICEIDS
declare -A APIKEY
include_config_or_exit join.sh

#
# send notification(s)
#
SUCCESS=0
for k in "${!APIKEY[@]}" ; do
  APIKEY="${APIKEY[$k]}"
  DEVICEIDS="${DEVICEIDS[$k]}"
  if [[ $DEVICEIDS == "" ]] ; then
    log "error - configuration missing device IDs for $k"
    SUCCESS=-1
  else
    this_success=$(curl -G -s --data-urlencode "title=$1" --data-urlencode "text=$2" "https://joinjoaomgcd.appspot.com/_ah/api/messaging/v1/sendPush?deviceIds=${DEVICEIDS}&apikey=${APIKEY}")
    if [[ $SUCCESS == 0 ]] ; then
      SUCCESS=`echo "$this_success" | grep success | grep -q true`
    fi
  fi
done

exit $SUCCESS
