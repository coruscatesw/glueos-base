#!/bin/bash
##
# This script sends notifications to clicksendsms via its api
#  arg1 = title of notification
#  arg2 = message in notification
#
# Returns 0 on success
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=2
REQ_BINARIES=( curl )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

title=$1
message=`echo $2`

#
# include configuration file
#
include_config_or_exit clicksendsms.sh


MESSAGE="$MSG_HEAD::$title:$message"

#
# create temp data file
#
TDF=/dev/shm/cs-tdf
cat > $TDF << EOF
{
  "messages":[
    {
      "source":"glueos",
      "body":"$MESSAGE",
      "to":"$DEST_NUM"
    }
  ]
}
EOF

#
# send notification
#
SUPPRESS_ME=$(curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Basic $AUTH_KEY" -d"@$TDF" "https://rest.clicksend.com/v3/sms/send")

SUCCESS=$?

exit $SUCCESS
