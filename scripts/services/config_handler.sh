#!/bin/bash

##
# This script handles file events in the glueos/config directory
#  arg1 = filename
## 

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# Handle the event, if the file still exists
#
if [ -e "$1" ] ; then
  log "handling $1"
else
  log "file does not exist $1"
  exit
fi

case "$1" in
*/config/*_sample.*)
  log "ignoring sample configuration file update"
  ;;
*/config/.*|*/config/*.swp|*/config/*.*.*)
  log "ignoring hidden, swap, or invalid file"
  ;;
*)
  FN=$(basename $1)
  if [[ "$FN" == *"."* ]] ; then
    log "dispatching config update to service monitor..."
    ${GLUEOS_SCRIPT_DIR}/local/service_monitor.sh "$1"
  else
    log "ignoring hidden, swap, or invalid file"
  fi
  ;;
esac
