#!/bin/bash

##
# This script handles temp events in the glueos/state/<room> directory
# It is called by the state_handler.sh
#   arg1 = filename that triggered update
##

#
# verify dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( grep )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# 
#
log "handling $1"

#
# This function handles updates to state files
#
handle_state_update() {
  # get room and temp/humidity from filename
  NULL_CMD=$(echo "$1" | grep temperature)
  IS_TEMP=$?
  VAL=$(cat "$1")
  split_string_on_delimiter "$1" "/"
  SET_ROOM=0

  for word in ${WORDS[@]} ; do
    if [[ $SET_ROOM == 1 ]] ; then
      ROOM="$word"
      SET_ROOM=0
      break
    fi
    if [[ "$word" == "room" ]] ; then
      SET_ROOM=1
    fi
  done

  TH=temperature
  if [[ $IS_TEMP != 0 ]] ; then
    TH=humidity
  fi

  if [[ -v ROOM ]] ; then
    log "sending temp manager update for $ROOM $TH $VAL"

    `dirname $0`/../temperature/temp_manager.sh "$ROOM" "$TH" "$VAL"
  else
    #outside temp/hum update
    log "outside temp updates not yet handled"
    #@TODO - call temp man for all controlled rooms
    # pass "outside" as room name
  fi
}

#
# This function handles updates to control files
#
handle_control_update() {
  troom=$(head -n1 "$1")
  tactive=$(tail -n1 "$1")
  log "temperature management $tactive for $troom"
  echo "$tactive" > `dirname $0`/../../state/room/${troom}/temp_control

  TF=`dirname $0`/../../state/room/${troom}/temperature
  if [ -e "$TF" ] ; then
    TEMP=$(cat "$TF")
    `dirname $0`/../temperature/temp_manager.sh "$troom" temperature $TEMP
  fi
}

case $1 in
*/state/*)
  log "handling state update for $1"
  handle_state_update "$1"
  ;;
*/control/*)
  log "handling control update for $1"
  handle_control_update "$1"
  rm -f "$1"
  ;;
*)
  log "unhandled $1"
  ;;
esac
