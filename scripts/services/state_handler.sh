#!/bin/bash

##
# This script handles file events in the glueos/state directory
#  arg1 = filename
## 

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# Handle the event, if the file still exists
#
if [ -e "$1" ] ; then
  log "handling $1"
else
  log "file does not exist $1"
  exit
fi

case "$1" in
*/state/device/*)
  log "dispatching to Device Handler..."
  `dirname $0`/device_handler.sh "$1"
  `dirname $0`/../network/remote_state_reporter.sh "$1"
  ;;
*/state/network/extip)
  log "external IP update, updating duck DNS..."
  `dirname $0`/../network/duck_dns_updater.sh
  `dirname $0`/../network/remote_state_reporter.sh "$1"
  ;;
*/state/network/*)
  log "network update..."
  `dirname $0`/../network/remote_state_reporter.sh "$1"
  ;;
*/state/*/temperature|*/state/*/humidity)
  log "dispatching to temp manager..."
  `dirname $0`/../services/temp_handler.sh "$1"
  `dirname $0`/../network/remote_state_reporter.sh "$1"
  ;;
*/state/service/zway_auth_cookie)
  log "received new zway auth cookie; getting device list..."
  `dirname $0`/../devices/zway.sh GET_DEVICES
  `dirname $0`/../network/remote_state_reporter.sh "$1"
  ;;
*/state/room/*)
  log "dispatching to remote state reporter..."
  `dirname $0`/../network/remote_state_reporter.sh "$1"
  ;;
*)
  log "no handler specified; ignoring"
  ;;
esac
