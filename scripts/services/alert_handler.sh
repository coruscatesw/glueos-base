#!/bin/bash

##
# This script handles alert events in the glueos/state/alert directory
# It is called by the state_handler.sh
#   arg1 = filename for alert
##

#
# verify dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( head tail )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
#
#
 log "handling $1"

if [ -e "$1" ] ; then
  TYPE=$(head -n1 "$1")
  TITLE=$(tail -n +2 "$1" | head -n1)
  TEXT=$(tail -n +3 "$1")

  log "sending alert <$TYPE> <$TITLE> <$TEXT>"
  `dirname $0`/../alerts/alerter.sh "$TYPE" "$TITLE" "$TEXT"
else
  log "file $1 does not exist"
  exit -1
fi

rm -f "$1"
