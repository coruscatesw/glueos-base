#!/bin/bash

##
# This script handles device events in the glueos/state/device directory
# It is called by the state_handler.sh
#   arg1 = filename for device
##

#
# verify dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
REQ_BINARIES=( )
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# read the device control file and pass along 
#
if [ -e "$1" ] ; then
  log "handling $1"
else
  log "error; file $1 does not exist"
  exit -1
fi
DEVICE_FILE="$1"

#
# This function controls devices
#
control_device () {
  DEVICE=$(head -n1 $DEVICE_FILE)
  STATE=$(tail -n1 $DEVICE_FILE)

  log "controlling device <$DEVICE> <$STATE>"
  `dirname $0`/../devices/device_control.sh "$DEVICE" "$STATE"
}

#
# This function queries devices
#
query_device() {
  # figure out, based on file name, what type of device to query
  split_string_on_delimiter "$DEVICE_FILE" "/"
  TYPE="unset"
  TRIGGER=0
  for word in ${WORDS[@]} ; do
    if [[ $TRIGGER == 1 ]] ; then
      TYPE=$(echo "$word" | cut -d'_' -f3)
      TRIGGER=0
    fi
    if [[ "$word" == "control" ]] ; then
      TRIGGER=1
    fi
  done

  if [[ $TYPE == "unset" ]] ; then
    log "query_device: Error; device type unset"
    return -1
  fi

  SPECIFIC_DEVICES=$(cat $DEVICE_FILE)
  if [[ $SPECIFIC_DEVICES == "" ]] ; then
    `dirname $0`/../devices/device_query.sh "$TYPE"
  else
    IFS=$'\n'
    for sdev in $SPECIFIC_DEVICES ; do
      `dirname $0`/../devices/device_query.sh "$TYPE" "$sdev"
    done
    IFS=$OLD_IFS
  fi
}


#
# figure out what type of control this is and dispatch
#
case $DEVICE_FILE in
*/device_query_*)
  log "performing device query..."
  query_device
  ;;
*/device_*)
  log "controlling device..."
  control_device
  ;;
*)
  log "unknown control; ignoring"
  ;;
esac

rm -rf $DEVICE_FILE

