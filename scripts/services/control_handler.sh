#!/bin/bash

##
# This script handles file events in the glueos/control directory
#  arg1 = filename
## 

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# Handle the event, if the file still exists
#
if [ -e "$1" ] ; then
  log "handling $1"
else
  log "file does not exist $1"
  exit
fi

case "$1" in
*/control/alert_*)
  log "dispatching to Alert Handler..."
  `dirname $0`/alert_handler.sh "$1"
  ;;
*/control/device_*)
  log "dispatching to Device Handler..."
  `dirname $0`/device_handler.sh "$1"
  ;;
*/control/temp_*)
  log "dispatching to Temp Handler..."
  `dirname $0`/temp_handler.sh "$1"
  ;;
*/control/hmi_*)
  log "dispatching to HMI Handler..."
  `dirname $0`/hmi_handler.sh "$1"
  ;;
*/control/wol_*)
  TARGET=$(cat $1)
  log "Sending WoL to $TARGET"
  `dirname $0`/../network/wake_on_lan.sh "$TARGET"
  ;;
*/control/duckdns_renewal_*)
  log "Renewing DuckDNS"
  `dirname $0`/../network/duck_dns_updater.sh
  ;;
*)
  log "no handler specified; ignoring"
  ;;
esac
