#!/bin/bash

##
# This script handles hmi events in the glueos/control directory
#  arg1 = filename
## 

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=1
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# Handle the event, if the file still exists
#
if [ -e "$1" ] ; then
  log "handling $1"
else
  log "file does not exist $1"
  exit
fi

HMI_QUERY=$(cat $1)
`dirname $0`/../hmi/hmi_manager.sh "$HMI_QUERY"

rm -f "$1"
