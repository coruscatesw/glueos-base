#!/bin/bash

##
# This is the main script for glueos.
##

#
# This function kicks off when the script exits. It should stop all child 
# processes that are still running
#
cleanup () {
  for pid in ${PROCS[@]} ; do
    if is_running "$pid" ; then
      kill $pid
    fi
  done
}

#
# This function starts a dispatcher type and stores its PID
#  arg1 = type [config|control|state]
#
start_dispatcher () {
  TYPE=$1
  ${homedir}/go/bin/dispatcher --dirs ${homedir}/glueos-base/$TYPE --handler ${homedir}/glueos-base/scripts/services/${TYPE}_handler.sh >> /var/log/glueos/${TYPE}.log 2>&1 &
  PROCS[${TYPE}]=$!
}

#
# This function checks whether a PID is running
#  arg1 = PID to check
#
# Returns 0 if running, -1 if not
#
is_running () {
  RET=0

  PID=$1
  if  [ "`ps -o pid= -p $PID`" = "" ]
  then
    # process is not running
    RET=-1
  fi

  return $RET
}

#
# Here starts the main code. Set up variables used by the script
#
declare -A PROCS
homedir=`dirname $0`/../../..
dispatchers=( config control state )

#
# start each type of dispatcher
#
for dtype in ${dispatchers[@]} ; do
  start_dispatcher $dtype
done

#
# ensure cleanup is done if this script is killed
#
trap cleanup 0 2 5 9 15

#
# enter a loop to monitor the dispatchers. If they fail for any reason, restart
# them. If all of them fail at the same time, exit. The expectation is that
# ctrl+c was pressed, which kills all child processs of this script
#
while [ 1 ]
do
  dead_dispatchers=()
  for dtype in ${dispatchers[@]} ; do
    if is_running "${PROCS[$dtype]}" ; then
      :
    else
      dead_dispatchers+=("$dtype")
    fi
  done

  if [[ ${#dead_dispatchers[@]} -gt 0 ]] ; then
    if [[ ${#dead_dispatchers[@]} -eq ${#dispatchers[@]} ]] ; then
      echo "glueos: all dispatchers have died; exiting"
      break
    else
      for dtype in ${dead_dispatchers[@]} ; do
        start_dispatcher $dtype
      done
    fi
  fi

  sleep 2
done

cleanup
