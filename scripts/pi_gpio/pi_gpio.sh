#!/bin/bash

##
# This script interacts with the Raspberry Pi GPIO pins.
# Raspberry Pi uses BCM pin numbers by default.
#   arg1  = command [EXPORT |
#                    UNEXPORT |
#                    GET_ACTIVE_LOW |
#                    SET_ACTIVE_LOW |
#                    GET_DIRECTION |
#                    SET_DIRECTION |
#                    GET_EDGE |
#                    SET_EDGE |
#                    GET_VALUE |
#                    SET_VALUE]
#   arg2  = Pin Number
#   arg3  [Required for "set" actions.]
#         = Value to set to.
##

GPIO_START_PATH="/sys/class/gpio"

#
# Verify all dependencies for this script
#
NUM_ARGS=$#
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#Functions to Handle Input
#
# This function handles exporting a pin
#
exportpin() {
  echo $1 > $GPIO_START_PATH/export 
}

#
# This function handles unexporting a pin
#
unexportpin() {
  echo $1 > $GPIO_START_PATH/unexport
}

#
# This function gets the active low value for a pin
#
getactivelow() {
  cat $GPIO_START_PATH/gpio$1/active_low
}

#
# This function sets the active low value for a pin
#
setactivelow() {
  echo $2 > $GPIO_START_PATH/gpio$1/active_low
}

#
# This function gets the direction for a pin
#
getdirection() {
  cat $GPIO_START_PATH/gpio$1/direction
}

#
# This function sets the direction for a pin
#
setdirection() {
  echo $2 > $GPIO_START_PATH/gpio$1/direction
}

#
# This function gets the edge for a pin
#
getedge() {
  cat $GPIO_START_PATH/gpio$1/edge
}

#
# This function sets the edge for a pin
#
setedge() {
  echo $2 > $GPIO_START_PATH/gpio$1/edge
}

#
# This function gets the value for a pin
#
getvalue() {
  cat $GPIO_START_PATH/gpio$1/value
}

#
# This function sets the value for a pin
#
setvalue() {
  if [[ $(getdirection $1) == "in" ]] || [[ $(getdirection $1) == "low" ]] ; then 
    log "Cannot set the value of an input pin (Pin #$1)"
    exit 2
  else
    echo $2 > $GPIO_START_PATH/gpio$1/value
  fi

}

if [[ $EUID -ne 0 ]]; then
  echo "This script needs to be run as root."
  exit 1
fi

#Call functions based on argument inputs.
case $1 in
  "EXPORT")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "Exporting Pin #$2"
    exportpin $2
    ;;
  "UNEXPORT")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "Unexporting Pin #$2"
    unexportpin $2
    ;;
  "GET_ACTIVE_LOW")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the active low value of Pin #$2"
    getactivelow $2
    ;;
  "SET_ACTIVE_LOW")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET Pin #$2 active low value to <$3>"
    setactivelow $2 $3
    ;;
  "GET_DIRECTION")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the direction of Pin #$2"
    getdirection $2
    ;;
  "SET_DIRECTION")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET Pin #$2 direction to <$3>"
    setdirection $2 $3
    ;;
  "GET_EDGE")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the edge value of Pin #$2"
    getedge $2
    ;;
  "SET_EDGE")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET Pin #$2 edge to <$3>"
    setedge $2 $3
    ;;
  "GET_VALUE")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the value of Pin #$2"
    getvalue $2
    ;;
  "SET_VALUE")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET Pin #$2 value to <$3>"
    setvalue $2 $3
    ;;
  *)
    log "Unhandled command specified - $1"
    ;;
esac
