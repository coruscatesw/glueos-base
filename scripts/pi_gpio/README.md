#GPIO Notes

In order for the PWM to work properly on a Raspberry Pi, some changes have to
be made to the boot configuration file.

The notes were gotten from
http://www.jumpnowtek.com/rpi/Using-the-Raspberry-Pi-Hardware-PWM-timers.html

However they're summarized below.

First find out your kernel version.
>uname -a

4.4 Instructions:
These are listed on the site above, assumed to work, but not tested
as this was developed on the 4.9 kernel.

4.9: Instructions:
0) See if PWM is already running on your pi
  "lsmod | grep pwm"
  If nothing is returned follow the below instructions.
1) Using your favorite editor, edit "/boot/config.txt"
2) At the bottom of the file add the following:
  2a) IF you have LESS than 40pin board
    add "dtoverlay=pwm"
  2b) IF you have GREATER than 40pin board
    add "dtoverlay=pwm-2chan"
3) IF 2a - This will default the PWM to channel 0 on pin 18
   IF 2b - This will set pin 18 to channel 0 and pin 19 to channel 1
4) Reboot the Pi
5) When rebooted verify pwm is running.
  "lsmod | grep pwm"
