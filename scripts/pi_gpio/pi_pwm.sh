#!/bin/bash

##
# This script interacts with the Raspberry Pi GPIO pins
# for Pulse Width modulation.
# This script is using the default Raspberry Pi implementation.
# Raspberry Pi uses BCM pin numbers by default.
# PWM is only on pin 18 for channel 0
# PWM is only on pin 19 for channel 1
#   arg1  = command [EXPORT |
#                    UNEXPORT |
#                    GET_DUTY_CYCLE |
#                    SET_DUTY_CYCLE |
#                    GET_ENABLE |
#                    SET_ENABLE |
#                    GET_PERIOD |
#                    SET_PERIOD |
#                    GET_POLARITY |
#                    SET_POLARITY]
#   arg2  = Channel #
#   arg3  [Required for "set" actions.]
#         = Value to set to.
##

#MAX_INT_SIZE=$(echo "(2**63)-1" | bc)
MAX_INT_SIZE=$(((2**63)-1))
PWM_START_PATH="/sys/class/pwm/pwmchip0"

#
# Verify all dependencies for this script
#
NUM_ARGS=$#
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#Functions to Handle Input
#
# This function exports the channel
#
exportchannel() {
  echo $1 > $PWM_START_PATH/export
}

#
# This function unexports the channel
#
unexportchannel() {
  echo $1 > $PWM_START_PATH/unexport
}

#
# This function gets the duty_cycle
#
getdutycycle() {
 cat $PWM_START_PATH/pwm$1/duty_cycle
}

#
# This function sets the duty_cycle
#
setdutycycle() {
  if [[ MAX_INT_SIZE > $2 ]] && [[ $2 > $(getperiod $1) ]];then
    echo $2 > $PWM_START_PATH/pwm$1/duty_cycle
  else
    log "Duty Cycle must be greater than period."
  fi
}

#
# This function gets if the channel is enabled
#
getenable() {
  cat $PWM_START_PATH/pwm$1/enable
}

#
# This function sets the channel to enabled
#
setenable() {
  echo $2 > $PWM_START_PATH/pwm$1/enable
}

#
# This function gets the period
#
getperiod() {
  cat $PWM_START_PATH/pwm$1/period
}

#
# This function sets the period
#
setperiod() {
  if [[ MAX_INT_SIZE > $2 ]]; then
    echo $2 > $PWM_START_PATH/pwm$1/period
  else
    log "Cannot set period greater than max int size."
  fi
}

#
# This function gets the polarity
#
getpolarity() {
  cat $PWM_START_PATH/pwm$1/polarity
}

#
# This function sets the polarity
#
setpolarity() {
  if [[ $(getenable $1) == "0" ]]; then
    echo $2 > $PWM_START_PATH/pwm$1/polarity
  else
    log "Cannot set the polarity while the channel is enabled."
  fi
}


if [[ $EUID -ne 0 ]]; then
  echo "This script needs to be run as root."
  exit 1
fi

#Call functions based on argument inputs.
case $1 in
  "EXPORT")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "Exporting Channel #$2"
    exportchannel $2
    ;;
  "UNEXPORT")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "Unexporting Channel #$2"
    unexportchannel $2
    ;;
  "GET_DUTY_CYCLE")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the duty cycle value of Channel #$2"
    getdutycycle $2
    ;;
  "SET_DUTY_CYCLE")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET channel #$2 duty cycle value to <$3>"
    setdutycycle $2 $3
    ;;
  "GET_ENABLE")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET is channel #$2 enabled"
    getenable $2
    ;;
  "SET_ENABLE")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    ENABLED_TEXT="not enabled"
    if [[ $3 == "1" ]]; then
      ENABLED_TEXT="enabled"
    fi
    log "SET channel #$2 to $ENABLED_TEXT"
    setenable $2 $3
    ;;
  "GET_PERIOD")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the period of channel #$2"
    getperiod $2
    ;;
  "SET_PERIOD")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET channel #$2 period to <$3>"
    setperiod $2 $3
    ;;
  "GET_POLATIRY")
    REQ_NUM_ARGS=2
    verify_args && exit_if_verification_failed
    log "GET the polarity of channel #$2"
    getpolarity $2
    ;;
  "SET_POLARITY")
    REQ_NUM_ARGS=3
    verify_args && exit_if_verification_failed
    log "SET channel #$2 value to <$3>"
    setpolarity $2 $3
    ;;
  *)
    log "Unhandled command specified - $1"
    ;;
esac
