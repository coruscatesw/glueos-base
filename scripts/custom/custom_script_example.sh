#!/bin/bash

# This script will be executed by the HMI manager when the input matches. See the
# hmi_sample.sh config file

# All this script does is write to a file in /tmp. It's only purpose is to show
# the user where to put custom scripts, and how to invoke them via the hmi manager

echo "custom script invoked at `date` with parameters $@" >> /tmp/custom_script.log
