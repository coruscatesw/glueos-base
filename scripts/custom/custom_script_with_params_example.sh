#!/bin/bash
##
# This is a custom script to show how parameters are passed/handled
#  arg1..N = a 'NAME=value' argument, likely passed from hmi_manager.sh
#
# Returns 0 if successful
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# main script code
#

# this must be declared prior to parsing our input
declare -A PARSED_VARS

# this stores our input in a PARSED_VARS array with the key being the name
# of the variable passed in. I.e. an argument of 'STATE=off' would set
# $PARSED_VARS["STATE"] to 'off'
set_vars_from_input "$@"

# for complete example, echo all of the key,value pairs passed in and output
for k in "${!PARSED_VARS[@]}" ; do
  v="${PARSED_VARS[$k]}"
  echo "key,value: <$k,$v>"
done
