#!/bin/bash

##
# This script sends alerts when a local login is established on this host. In
# order to work, create a symbolic link to this script in /etc/profile.d/
#   cd /etc/profile.d; sudo ln -s /home/myuser/glueos-base/scripts/local/login_notify.sh
##

# determine the actual location of the script. Required to determine relative
# locations of glueOS components
ME=$(readlink $0)
if [[ $? != 0 ]] ; then
  ME="$0"
fi

#
#
. `dirname $ME`/../general/verify_dependencies.sh
. `dirname $ME`/../general/globals.sh "`dirname $ME`/../../"

#
#
include_config_or_exit local_login.sh

#
# This function verifies the user logged in against the config
#
verify_user() {
  ret=0

  for usr in "${EXCLUDE_USERS[@]}" ; do
    if [[ "$usr" == "${USER}" ]] ; then
      ret=-1
      logState "ignoring local login from <$USER>"
      break
    fi
  done

  return $ret
}

#
# main
# 
log "${USER} logged in"
verify_user && alertSecurity "${USER} logged in"
