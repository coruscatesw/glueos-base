#!/bin/bash
##
# This script monitors hosts on the network and alerts if they become unavailable
#
# Returns 0
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
REQ_NUM_ARGS=0
REQ_BINARIES=( docker )
. `dirname $0`/../general/verify_dependencies.sh
# include globals; pass reference to glueos-base directory
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# include configuration file that contains alerter
#
include_config_or_exit docker_container_monitor.sh

#
# ping the host(s)
#
for container in $CONTAINERS_TO_MONITOR
do
  CMD=$(sudo docker ps -a | grep $container | grep Up)
  if [[ $? != 0 ]] ; then
    alertError "Container unavailable" "$container down as of `date +%s`"
    echo "sent alert for container $container"
  else
    echo "Container $container good"
  fi
done
