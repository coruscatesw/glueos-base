#!/bin/bash
##
# This script checks the config directory and sets the state of each service
# based on whether a config exists. Services with no config cannot run
#  arg1 = (optional) name of file that triggered update
#
# Returns 0
##

#
# verify all dependencies for this script
#
NUM_ARGS=$#
#REQ_NUM_ARGS=2   # uncomment to enforce the number of command line args
#REQ_BINARIES=( ) # uncomment to require specific binaries for execution
#REQ_PACKAGES=( ) # uncomment to require specific packages for execution
. `dirname $0`/../general/verify_dependencies.sh
. `dirname $0`/../general/globals.sh "`dirname $0`/../../"

#
# This function updates the state of a service based on whether it has a config
#  arg1 = service name
determine_service_state() {
  if [ -e "${GLUEOS_CONFIG_DIR}/$1.sh" ] ; then
    echo "active" > "${GLUEOS_STATE_DIR}/service/$1"
  else
    echo "inactive" > "${GLUEOS_STATE_DIR}/service/$1"
  fi
}

#
# main script code
#
services_to_check=( alerts autoremote devices dht22_rpi dnsmasq duck_dns hmi host_monitor join local_login remote_state_reporter ssh_notify temp_management temp_outside wake_on_lan webui zway )

if [ $# == 1 ] ; then
  service=$(echo "`basename $1`" | sed 's/\.[^\.]*$//')

  # verify this service is in our list of services to monitor
  SHOULD_STATUS="no"
  for stc in "${services_to_check[@]}" ; do
    if [[ "$stc" == "$service" ]] ; then
      SHOULD_STATUS="yes"
      break
    fi
  done

  if [[ "$SHOULD_STATUS" == "yes" ]] ; then
    log "only checking <$service>"
    services_to_check=( $service )
  else
    log "not checking invalid service <$service>"
    exit
  fi
else
  log "checking all services"
fi

for stc in "${services_to_check[@]}" ; do
  determine_service_state "$stc"
done
