# GlueOS Base

Base repository for GlueOS. This repository contains all scripts and other text files (i.e. not compilable source code).


# Setup

The current version of GlueOS is intended to be overlayed on Raspbian and run on Raspberry Pis. These instructions detail how to overlay a fresh Raspbian Stretch Lite install. The majority of GlueOS has also been tested on Ubuntu 17.10, but compatibility is not guaranteed.

(assuming a fresh image of stretch && raspi-config has already been run and they system has been configured)


Update repo list and all packages:

sudo apt-get update && sudo apt-get upgrade -y


Install necessary reqs for GlueOS:

sudo apt-get install golang git jq weather-util uuid-runtime dnsutils at bc php-cli


(Optional tools install):

sudo apt-get install vim


Edit bashrc to set GOPATH (i.e. 'vim ~/.bashrc' and append the following line):

export GOPATH=$HOME/go


Make the GOPATH change effective in current terminal:

. ~/.bashrc


Pull down the GlueOS Base repository:

git clone https://bitbucket.org/coruscatesw/glueos-base.git


Setup Go environment for GlueOS Go repository:

mkdir -p ~/go/src

cd ~/go/src

go get gopkg.in/fatih/set.v0
go get -u github.com/rjeczalik/notify


Pull down the GlueOS Go repository & build:

git clone https://bitbucket.org/coruscatesw/glueos-go.git

cd glueos-go/dispatcher

go install


The environment is now all set up for GlueOS. The next steps are configuring GlueOS for use.


## Configuring for use

./config/crontab_sample.txt shows examples of cron entries for glueos services. Set up your cron entries for all services desired. Like-wise, add configuration files for those services. The *_sample.* files in the config directory should be used as a base.

Some services in glueos rely on external applications. Instructions on usage for those is located in ./scripts/3rd_party.md


## Logging

Logs are written to /var/log/glueos/*.log. In order to provide write access to this directory:


As root: mkdir /var/log/glueos; chown myuser.myuser /var/log/glueos
